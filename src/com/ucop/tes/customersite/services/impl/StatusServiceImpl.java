package com.ucop.tes.customersite.services.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.ucop.tes.customersite.domain.BatchProcessQueueRecord;
import com.ucop.tes.customersite.domain.SchoolElectronicSubmissionRecord;
import com.ucop.tes.customersite.domain.StatusRecord;
import com.ucop.tes.customersite.domain.StatusRequestRecord;
import com.ucop.tes.customersite.domain.ThresholdRecord;
import com.ucop.tes.customersite.repository.SchoolDao;
import com.ucop.tes.customersite.services.StatusService;

@Stateless
public class StatusServiceImpl implements StatusService {

	@EJB
	private SchoolDao schoolDao;
	
	@Override
	public List<SchoolElectronicSubmissionRecord> getFileStatus(List<StatusRequestRecord> requestRecord) {
		return schoolDao.getSubmissions(requestRecord);
	}

	@Override
	public List<BatchProcessQueueRecord> getProcessStatus(List<StatusRequestRecord> requestRecord) {
		return schoolDao.getBatchStatus(requestRecord);
	}

	@Override
	public List<ThresholdRecord> getThreshold(List<StatusRequestRecord> requestRecord) {
		return schoolDao.getThreshold(requestRecord);
	}

	//MEK TES-131  
	@Override
	public List<StatusRecord> getStatus(List<StatusRequestRecord> requestRecord) throws Exception{
		
		List<StatusRecord> statusRecord = null;
		try{
			statusRecord = schoolDao.getStatus(requestRecord);
		} catch (Exception e){
			throw new Exception(e.getMessage());
		}

		//return schoolDao.getStatus(requestRecord);
		return statusRecord;
	}

	@Override
	public List<BatchProcessQueueRecord> getAccessDbStauts(String[] atpCode) {
		
		return schoolDao.getAccessDbStatus(atpCode);
	}
	
}
