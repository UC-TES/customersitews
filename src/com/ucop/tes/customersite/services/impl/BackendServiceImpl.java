package com.ucop.tes.customersite.services.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.ucop.tes.customersite.common.CNCParameters;
import com.ucop.tes.customersite.common.CommonNameConstants;
import com.ucop.tes.customersite.common.StringUtils;
import com.ucop.tes.customersite.config.Config;
import com.ucop.tes.customersite.domain.CommonNameCreationDto;
import com.ucop.tes.customersite.domain.ExpandedCourseNames;
import com.ucop.tes.customersite.domain.SchoolElectronicSubmissionRecord;
import com.ucop.tes.customersite.dto.UploadFileValidatorMessageDTO;
import com.ucop.tes.customersite.repository.AccessDbDao;
import com.ucop.tes.customersite.repository.CommonNameCreationDao;
import com.ucop.tes.customersite.repository.SchoolDao;
import com.ucop.tes.customersite.services.AwsService;
import com.ucop.tes.customersite.services.BackendService;

@Stateless
public class BackendServiceImpl implements BackendService {

	private static Logger logger = Logger.getLogger(BackendServiceImpl.class);
	
	@EJB
	private AccessDbDao accessDbDao;
	
	@EJB
	private CommonNameCreationDao commonNameCreationDao;
	
	@EJB
	private AwsService awsService;
	
	@EJB
	private SchoolDao ssDao;
	
	private HashMap<String, String> hmAbbrevExpand;

	@Override
	public void createMSAccessDB(String[] atpCode) {
		accessDbDao.createMSAccessDB(atpCode);

	}

	@Override
	public String commonNameCreator() {
		//TreeMap sourceCommon = new TreeMap();
		String returnVal = null;
		String sourceName = null;
		
		List<CommonNameCreationDto> sourceNames = new ArrayList<CommonNameCreationDto>();
		List<ExpandedCourseNames> expandedNames = new ArrayList<ExpandedCourseNames>();
		try {
			sourceNames = commonNameCreationDao.getCommonNameCreation();
			expandedNames = commonNameCreationDao.getExpandedNames();
			
			hmAbbrevExpand = new HashMap<String, String>();
			//build hashmap from list
			for(ExpandedCourseNames dto : expandedNames){
				hmAbbrevExpand.put(dto.getAbbrevCourseName().trim().toUpperCase(), dto.getExpandCourseName());
			}
			
			
		} catch (Exception ex) {
			returnVal = "Could Not Load Course Names";
			logger.error("BackendServiceImpl.commonNameCreator() - Error loading common course names", ex);
		}
		
		//for (Iterator itSourceName = sourceNames.iterator(); itSourceName.hasNext();) {
		//	String sourceName = (String) itSourceName.next();
		//	if (sourceName == null || sourceName.trim().length() == 0) {
		//		continue;
		//	} // end if
		try {
			
			for(CommonNameCreationDto dto: sourceNames){
					
				sourceName = dto.getSourceCourseName();
					
				if (sourceName == null || sourceName.trim().length() == 0) {
					continue;
				} // end if
				
				String tmpSourceName = sourceName;
				if (StringUtils.containsNonAscii(sourceName)) {
					tmpSourceName = sourceName.replaceAll(
							CommonNameConstants.REGEX_NON_ASCII,
							CommonNameConstants.EMPTY_STRING);
				} // end if

			
				String commonName = createCommonName(tmpSourceName);
				
				commonNameCreationDao.createCommonName(sourceName.trim().toUpperCase(),commonName.toUpperCase());
				
				returnVal = "success";
				
			} // end for
			
		} catch (Exception ex) {
			returnVal = "Could Not create Common Names"; 
			logger.error("BackendServiceImpl.commonNameCreator() - Error creating commmon name for '" + sourceName + "'", ex);
			
		} // end try
		
		return returnVal;

	}
	
	/*
	 * MEK This is the method that will copy the excel file from S3 TO the Linux /mnt/csv/<atpcode>
	 * 1.) copy excel file from S3 to /mnt/csv/<atpcode>
	 * 2.) Call stored proc to do the bulk load of the excel file to SQL Server. This might go into a process outside this web call
	 * based on how long the bulk loader takes. Same with 2.) below
	 * 3.) Once the call is done to the stored proc, remove the downloaded excel file from /mnt/csv/<atpcode>.
	 * @see com.schoolzilla.uc.services.service.BackendService#fileUploadValidator(java.lang.String)
	 */
	@Override
	public String fileUploadValidator(UploadFileValidatorMessageDTO dto) {

		String rtn = null;
		//List<byte[]> byteArrayList = new ArrayList<byte[]>();
		String status = null;
		try {
			
			//awsService.copyFileFromAwsS3(dto.getAtpCode(), dto.getFileName());
			
			
			//MEK TEFP-51 legacy text to excel conversion
			if(dto.getFileName().get(0).endsWith("txt")) {
				//here we store legacy EXCEL file name in the school submissions table
				//String copyToDirectory = Config.getStringProperty("app.copy.dir");
				
				dto.setExcelFileName("legacy" + dto.getAtpCode() + ".xlsx");
				
				status = "LEGACY";
			} else {
				status = "NEW_EXCEL";
				dto.setExcelFileName(dto.getFileName().get(0));
			}
			
			
			//MEK TEFP-39
			List<SchoolElectronicSubmissionRecord> ssRecord = ssDao.createSchoolSubmissionRecord(dto, status);
			
			
			//MEK if ssRecord does NOT exist, then ATP Code invalid and return ERROR as per Customer Site requirements
			if(ssRecord != null && ssRecord.size() > 0) {
				dto.setSubmissionId(ssRecord.get(0).getAutoElectronicSubmissionsID());
				
				
				this.postToQueue(dto); 
				
				//MEK TEFP-39
				rtn = ssRecord.get(0).getSystemUuId();
				
				logger.debug("fileUploadValidator System UUID is: " + rtn);
				
			} else {
				rtn = "ERROR";
			}
			
		} catch(Exception e){
			rtn = e.getMessage();
			
			logger.error("BackendServiceImpl.fileUploadValidator() - Exception: ", e);
			
		}
		
		return rtn;
		
	}
	
	/*
	 * Delete uploaded excel file from the share directory (/mnt/csv/<asp code>
	*/
	@Override
	public boolean deleteShareDirectoryFile(String fileName){
		boolean isDeleted = false;
		File f = new File(fileName);
		if(f.exists()){
			f.delete();
			isDeleted = true;
		}
		
		return isDeleted;
	}

	private String createCommonName(String sourceName) throws Exception {
		// 1. removing - remove nonLetters & nonNumbers
		// 2. go through Abbrev Rules for expanding
		// 3. go through Synch Rules for Synching
		// 4. reversing - move Sysched Charaters from the beginning of the name
		// to the end
		// 5. shrinking - remove all white space

		StringBuffer sbCommonName = new StringBuffer();

		// 1. replace non-letter/non-number/non-space that is not between
		// numbers with space
		String tmpName = StringUtils.removeSpecialCharacters(sourceName.trim());
		if (tmpName.trim().length() == 0) {
			throw new Exception("Source Name is a Empty String!");
		} // end if

		// split name by spaces
		String[] nameSplits = tmpName.split(CommonNameConstants.REGEX_SPACES);

		// 2. expand and 3. synch
		for (int i = 0; i < nameSplits.length; i++) {
			if (nameSplits[i].trim().length() == 0
					|| (nameSplits[i].trim().equalsIgnoreCase("and") && isToRemove(
							nameSplits, i))) {
				continue;
			} // end if

			String key = nameSplits[i].trim().toUpperCase();
			String tmpNameExpanded = (String) hmAbbrevExpand.get(key);
			String tmpNameSynched = (String) CNCParameters.getHmSynch()
					.get(key);

			if (tmpNameExpanded != null && tmpNameExpanded.trim().length() > 0) { // expand
																					// abbrev
																					// with
																					// full
																					// name
				sbCommonName.append(tmpNameExpanded).append(
						CommonNameConstants.SPACE);
			} else if (tmpNameSynched != null
					&& tmpNameSynched.trim().length() > 0) { // synch name
				sbCommonName.append(tmpNameSynched).append(
						CommonNameConstants.SPACE);
			} else {
				sbCommonName.append(nameSplits[i]).append(
						CommonNameConstants.SPACE);
			} // end if
		} // end for (int i = 0; i < nameSplits.length; i++)

		// 4. reversing & 5. shrinking - remove space(s) that is not between
		// numbers.
		return StringUtils.removeSpaces(StringUtils
				.reverseSynchedCharacters(sbCommonName.toString()));
	}

	private boolean isToRemove(String[] words, int index) {
		if (words == null) {
			return true;
		} // end if

		if (index == 0 || index == words.length - 1) {
			return false;
		} // end if

		// only remove word "and" when it is NOT between two numbers
		if (StringUtils.isNumber(words[index - 1])
				&& StringUtils.isNumber(words[index + 1])) {
			return false;
		} // end if

		return true;
	}
	
	
	/*
	 * Post message to Upload File Validation
	 * MEK TEFP-7
	 * MEK TEFP - 42 we no longer do this...but let's leave it just incase the Stored Procedure deadlock issue is ever fixed.
	 */
	private void postToQueue(UploadFileValidatorMessageDTO dto) {
		
		String validateUploadURL = Config.getStringProperty("app.validate.file.url");
		
		logger.debug("Sending Upload File Validation request: " + validateUploadURL);
		
		ClientBuilder.newClient()
					 .target(validateUploadURL)
					 .request(MediaType.APPLICATION_JSON)
					 .accept(MediaType.APPLICATION_JSON)
					 .post(Entity.entity(dto, MediaType.APPLICATION_JSON), UploadFileValidatorMessageDTO.class);
		
		/*
		 * HttpHeaders headers = new HttpHeaders(); headers.add("Accepts",
		 * MediaType.APPLICATION_JSON_VALUE);
		 * headers.setContentType(MediaType.APPLICATION_JSON);
		 * 
		 * RestTemplate restTemplate = new RestTemplate();
		 * 
		 * HttpEntity<UploadFileValidatorMessageDTO> requestBody = new HttpEntity<>(dto,
		 * headers); restTemplate.postForObject(validateUploadURL, requestBody,
		 * UploadFileValidatorMessageDTO.class);
		 */
	
	}
	
}