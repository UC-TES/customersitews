package com.ucop.tes.customersite.services.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;

import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

import com.ucop.tes.customersite.config.Config;
import com.ucop.tes.customersite.services.AwsService;

@Stateless
public class AwsServiceImpl implements AwsService{

	private static Logger logger = Logger.getLogger(AwsServiceImpl.class);
	
	private static String awsAccessKey;
	private static String awsSecretKey;
	private static String awsBucketName;
	private static String appEnv;
	private static String schoolAtpCode;
	
	@Override
	public void copyFileToAwsS3(File file, String atpCode) {
		awsAccessKey = Config.getStringProperty("AWS.ACCESS.KEY");
		awsSecretKey = Config.getStringProperty("AWS.SECRET.KEY");
		awsBucketName = Config.getStringProperty("AWS.BUCKET.NAME");
		appEnv = Config.getStringProperty("app.environment");
		
		schoolAtpCode = atpCode;
		
		String fileName = atpCode + ".mdb";
		
		String awsPath = "accessDb" + "/" + appEnv + "/" + schoolAtpCode + "/" +  fileName;
		
		try{
			logger.info(awsPath);
			System.out.println(">>> Uploading to AWS: " + awsPath);
			
			createS3client().putObject(awsBucketName,awsPath,file);
		} catch (Exception e) {
			logger.error(e.getMessage());
		} 
		
	}

	/**
	 * This method copies a file from an S3 bucket to a drive on the machine where this runs
	 */
	@Override
	public byte[] copyFileFromAwsS3(String atpCode, String fileName) throws Exception {
		
		awsAccessKey = Config.getStringProperty("AWS.ACCESS.KEY");
		awsSecretKey = Config.getStringProperty("AWS.SECRET.KEY");
		awsBucketName = Config.getStringProperty("AWS.BUCKET.NAME");
		appEnv = Config.getStringProperty("app.environment");
		
		//String aswKey = null;
				
		String awsPath =  appEnv + "/" + atpCode + "/" +  fileName;
		logger.info("*************AWS PATH: " + awsPath );
		
		byte[] buffer = new byte[8192];
	    int bytesRead;
	    ByteArrayOutputStream output = new ByteArrayOutputStream();
		
		try {
			
			S3Object fetchFile = createS3client(awsAccessKey,awsSecretKey).getObject(new GetObjectRequest(awsBucketName, awsPath));
			
			//final BufferedInputStream i = new BufferedInputStream(fetchFile.getObjectContent());
			InputStream objectData = fetchFile.getObjectContent();
			
			while ((bytesRead = objectData.read(buffer)) != -1)
		    {
		        output.write(buffer, 0, bytesRead);
		    }
			
			
			/*File files = new File(Config.getStringProperty("app.copy.dir") + atpCode);
			
	        if (!files.exists()) {
	            if (!files.mkdirs()) {
	            	logger.error("Cannot create directory: " + Config.getStringProperty("app.copy.dir") + atpCode);
	            	throw new Exception("Cannot create directory: " + Config.getStringProperty("app.copy.dir") + atpCode);
	            }
	        }
			
			Files.copy(objectData, new File(Config.getStringProperty("app.copy.dir") + atpCode + "/" + fileName).toPath(),java.nio.file.StandardCopyOption.REPLACE_EXISTING);
			*/
			objectData.close();
			return output.toByteArray();			
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			
			throw new Exception(e.getMessage());
		} 
		
	}
	
	private AmazonS3Client createS3client(String AccessKey, String SecretKey ){
		AWSCredentials credentials=new BasicAWSCredentials(AccessKey,SecretKey);
		AmazonS3Client client = new AmazonS3Client(credentials);
		
		return client;
	}
	
	private AmazonS3Client createS3client(){
		AWSCredentials credentials=new BasicAWSCredentials(awsAccessKey,awsSecretKey);
		AmazonS3Client client = new AmazonS3Client(credentials);
		
		return client;
	}

}
