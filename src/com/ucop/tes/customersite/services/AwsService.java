package com.ucop.tes.customersite.services;

import java.io.File;

public interface AwsService {
	
	void copyFileToAwsS3(File file, String atpCode);
	byte[] copyFileFromAwsS3(String atpCode, String fileName) throws Exception;

}
