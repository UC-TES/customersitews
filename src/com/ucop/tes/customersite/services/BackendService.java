package com.ucop.tes.customersite.services;

import com.ucop.tes.customersite.dto.UploadFileValidatorMessageDTO;

public interface BackendService {
	
	void createMSAccessDB(String[] atpCode);
	String commonNameCreator();
	
	//MEK this is for our NEW file upload validator.  For now just return void
	String fileUploadValidator(UploadFileValidatorMessageDTO dto);
	boolean deleteShareDirectoryFile(String fileName);
}
