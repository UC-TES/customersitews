package com.ucop.tes.customersite.services;

import java.util.List;

import com.ucop.tes.customersite.domain.BatchProcessQueueRecord;
import com.ucop.tes.customersite.domain.SchoolElectronicSubmissionRecord;
import com.ucop.tes.customersite.domain.StatusRecord;
import com.ucop.tes.customersite.domain.StatusRequestRecord;
import com.ucop.tes.customersite.domain.ThresholdRecord;


public interface StatusService {
	List<SchoolElectronicSubmissionRecord> getFileStatus(List<StatusRequestRecord> requestRecord);
	List<BatchProcessQueueRecord> getProcessStatus(List<StatusRequestRecord> requestRecord);
	List<ThresholdRecord> getThreshold(List<StatusRequestRecord> requestRecord);
	//MEK TES-131 
	List<StatusRecord> getStatus(List<StatusRequestRecord> requestRecord) throws Exception;
	List<BatchProcessQueueRecord> getAccessDbStauts(String[] atpCode);
}
