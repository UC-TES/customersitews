package com.ucop.tes.customersite.repository;

import com.ucop.tes.customersite.domain.PollerQueueDto;
import com.ucop.tes.customersite.domain.PollerStatusDto;

public interface BatchtProcessStatusDao {

	public void cleanJobStatus(String atpCode)throws Exception;
	
	public PollerQueueDto insertJobStatus(String atpCode) throws Exception;
	
	public void updateJobStatus(PollerQueueDto jobStatus)throws Exception;
	
	public PollerStatusDto insertJobStep(PollerStatusDto dto) throws Exception;
	
	public void updateJobStep(PollerStatusDto dto) throws Exception;
	
	
}
