package com.ucop.tes.customersite.repository;

import java.util.List;

import com.ucop.tes.customersite.domain.CommonNameCreationDto;
import com.ucop.tes.customersite.domain.ExpandedCourseNames;

public interface CommonNameCreationDao {
	
	List<CommonNameCreationDto> getCommonNameCreation();
	
	void createCommonName(String sourceName, String commonName);
	
	List<ExpandedCourseNames> getExpandedNames();

}
