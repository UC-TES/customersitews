package com.ucop.tes.customersite.repository;

import java.util.List;

import com.ucop.tes.customersite.domain.BatchProcessQueueRecord;
import com.ucop.tes.customersite.domain.SchoolElectronicSubmissionRecord;
import com.ucop.tes.customersite.domain.StatusRecord;
import com.ucop.tes.customersite.domain.StatusRequestRecord;
import com.ucop.tes.customersite.domain.ThresholdRecord;
import com.ucop.tes.customersite.dto.UploadFileValidatorMessageDTO;

public interface SchoolDao {

	List<SchoolElectronicSubmissionRecord> getSubmissions(List<StatusRequestRecord> requestRecord);
	
	List<BatchProcessQueueRecord> getBatchStatus(List<StatusRequestRecord> requestRecord);
	
	List<ThresholdRecord> getThreshold(List<StatusRequestRecord> requestRecord);
		
	List<StatusRecord> getStatus(List<StatusRequestRecord> requestRecord);
	
	List<BatchProcessQueueRecord> getAccessDbStatus(String[] atpCode);
	
	List<SchoolElectronicSubmissionRecord> createSchoolSubmissionRecord(UploadFileValidatorMessageDTO dto, String status);
}
