package com.ucop.tes.customersite.repository.jdbc;

import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

@Stateless
public class GenericDao {
	
	//@Resource(lookup = "TESY10") 
	@Resource(lookup = "jdbc/TESY10") 
	private DataSource dataSource;
	
	public GenericDao() {}
	
	protected Connection getConnection() {
		Connection connection = null;
		
		try {
			connection = dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return connection;
	}
}