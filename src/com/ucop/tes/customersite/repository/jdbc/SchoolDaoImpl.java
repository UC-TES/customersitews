package com.ucop.tes.customersite.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.*;

import org.apache.log4j.Logger;

import com.ucop.tes.customersite.domain.BatchProcessQueueRecord;
import com.ucop.tes.customersite.domain.BatchProcessStatusRecord;
import com.ucop.tes.customersite.domain.SchoolElectronicSubmissionRecord;
import com.ucop.tes.customersite.domain.StatusRecord;
import com.ucop.tes.customersite.domain.StatusRequestRecord;
import com.ucop.tes.customersite.domain.ThresholdRecord;
import com.ucop.tes.customersite.dto.UploadFileValidatorMessageDTO;
import com.ucop.tes.customersite.repository.SchoolDao;

@Stateless
//@TransactionManagement(TransactionManagementType.CONTAINER)
public class SchoolDaoImpl extends GenericDao implements SchoolDao {
	private static Logger logger = Logger.getLogger(SchoolDaoImpl.class);
	
	static final String STATUS_QRY = "SELECT ProcessStatusID, JobID, ProcessName, Status,"
			+ "SuccessRecordCount, ErrorRecordCount, StartTime, EndTime"
			+ " FROM BatchProcessStatus where JobId = ?";
	
	static final String VALID_ATP_CODE = "select schoolATPCode, threshold from tesSchool where schoolATPCode = ?";
	
	static List<String> aptCodeParams;
	
	static List<String> uuIdParams;
	
	@Override
	public List<SchoolElectronicSubmissionRecord> getSubmissions(List<StatusRequestRecord> requestRecord) {
		List<SchoolElectronicSubmissionRecord> result = new ArrayList<>();
		
		this.inClauseBuilder(requestRecord);
		
		String sqlQuery = new StringBuilder()
				.append("SELECT autoElectronicSubmissionsID, schoolATPCode, dateSubmitted, fileName, numberOfTranscripts, loadedFlag, dateLoaded, processingStatus, Submission_UUID")
				.append(" FROM schoolElectronicSubmissions")
				.append(" WHERE schoolATPCode IN (")
				.append(createInClauseParams(aptCodeParams))
				.append(")")
				.append(" AND Submission_UUID IN (")
				.append(createInClauseParams(uuIdParams))
				.append(")")
				.toString();
		
		logger.debug(String.format("Executing prepared SQL statement [%s]", sqlQuery));
		
		try (Connection conn = super.getConnection();
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);) {
		
			List<String> parameters = new ArrayList<>();
			parameters.addAll(aptCodeParams);
			parameters.addAll(uuIdParams);
			
			setStatementParameters(stmt, parameters);
			
			try (ResultSet rs = stmt.executeQuery();) {
				while (rs.next()) {
					SchoolElectronicSubmissionRecord entity = new SchoolElectronicSubmissionRecord();
					entity.setAtpCode(rs.getString("schoolATPCode"));
					//entity.setAdminContactEmail(rs.getString("adminContactEmail"));
					//entity.setAdminContactFName(rs.getString("adminContactFName"));
					//entity.setAdminContactLName(rs.getString("adminContactLName"));
					entity.setAutoElectronicSubmissionsID(rs.getInt("autoElectronicSubmissionsID"));
					entity.setProcessingStatus(rs.getString("processingStatus"));
					entity.setDateLoaded(rs.getDate("dateLoaded"));
					entity.setDateSubmitted(rs.getDate("dateSubmitted"));
					//entity.setExtractVendorName(rs.getString("extractVendorName"));
					//entity.setExtractVendorPhone(rs.getString("adminContactPhone"));
					entity.setFileName(rs.getString("fileName"));
					//entity.setLayoutVersion(rs.getString("layoutVersion"));
					entity.setLoadedFlag(rs.getString("loadedFlag"));
					entity.setNumberOfTranscripts(rs.getInt("numberOfTranscripts"));
					//entity.setProgramId(rs.getString("programID"));
					//entity.setSisInfo(rs.getString("SISInfo"));
					//entity.setSisType(rs.getString("SISType"));
					entity.setSystemUuId(rs.getString("Submission_UUID"));

					result.add(entity);
				}
			}
		} 
		catch (Exception e) {
			String msg = "SchoolDaoImpl.getSubmissions() SQL Exception in RowMapper: " + e.getMessage();
					
			logger.error(msg, e);
		} 

		return result;
	}
	
	@Override
	public List<BatchProcessQueueRecord> getBatchStatus(List<StatusRequestRecord> requestRecord) {
		List<BatchProcessQueueRecord> result = new ArrayList<>();
		
		this.inClauseBuilder(requestRecord);
		
		String sqlQuery = new StringBuilder()
				.append("SELECT JobId, schoolATPCode, JobStatus, Requester, RecordCount, UploadTime, BatchCompletionTime, Submission_UUID")
				.append(" FROM BatchProcessQueue")
				.append(" WHERE schoolATPCode IN (")
				.append(createInClauseParams(aptCodeParams))
				.append(")")
				.append(" AND Submission_UUID IN (")
				.append(createInClauseParams(uuIdParams))
				.append(")")
				.toString();
		
		logger.debug(String.format("Executing prepared SQL statement [%s]", sqlQuery));
		
		try (Connection conn = super.getConnection();
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);) {
			
			List<String> parameters = new ArrayList<>();
			parameters.addAll(aptCodeParams);
			parameters.addAll(uuIdParams);
			
			setStatementParameters(stmt, parameters);
		
			try (ResultSet rs = stmt.executeQuery();) {
				while (rs.next()) {
					BatchProcessQueueRecord entity = new BatchProcessQueueRecord();
					
					entity.setAtpCode(rs.getString("schoolATPCode"));
					entity.setBatchCompletionTime(rs.getDate("BatchCompletionTime"));
					entity.setJobId(rs.getInt("JobId"));
					entity.setJobStatus(rs.getString("JobStatus"));
					entity.setRecordCount(rs.getInt("RecordCount"));
					entity.setRequester(rs.getString("Requester"));
					entity.setUpLoadTime(rs.getDate("UploadTime"));
					entity.setSystemUuId(rs.getString("Submission_UUID"));

					result.add(entity);
				}
			}
		} 
		catch (Exception e) {
			String msg = "SchoolDaoImpl.getBatchStatus() SQL Exception in RowMapper: " + e.getMessage();
					
			logger.error(msg, e);
		} 
		
		for(BatchProcessQueueRecord each: result){
			each.setBatchStatus(this.getBatchProcessStatus(each.getJobId()));
		}

		return result;
	}
	
	@Override
	public List<ThresholdRecord> getThreshold(List<StatusRequestRecord> requestRecord) {
		List<ThresholdRecord> result = new ArrayList<>();
		
		this.inClauseBuilder(requestRecord);
		
		String sqlQuery = new StringBuilder()
				.append("select schoolATPCode, threshold from tesSchool where schoolATPCode in (")
				.append(createInClauseParams(aptCodeParams))
				.append(")")
				.toString();
		
		logger.debug(String.format("Executing prepared SQL statement [%s]", sqlQuery));
		
		try (Connection conn = super.getConnection();
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);) {
		
			setStatementParameters(stmt, aptCodeParams);
			
			try (ResultSet rs = stmt.executeQuery();) {
				while (rs.next()) {
					ThresholdRecord entity = new ThresholdRecord();
					
					String strThreshold = rs.getString("threshold");
					
					if(strThreshold != null && !strThreshold.isEmpty()){
						int threshold = Integer.valueOf(strThreshold);
						//strThreshold = String.valueOf(threshold * 100);
						entity.setMatchingThresholdNumeric(threshold);
						entity.setMatchingThresholdSymbolic(strThreshold + "%");
						entity.setSchoolAtpCode(rs.getString("schoolATPcode"));
					}

					result.add(entity);
				}
			}
		} 
		catch (Exception e) {
			String msg = "SchoolDaoImpl.getThreshold() SQL Exception in RowMapper: " + e.getMessage();
					
			logger.error(msg, e);
		} 
		
		return result;
	}
	
	@Override
	public List<StatusRecord> getStatus(List<StatusRequestRecord> requestRecord) {
		List<SchoolElectronicSubmissionRecord> submissionRecords = getSubmissions(requestRecord);
		List<BatchProcessQueueRecord> queueRecords = getBatchStatus(requestRecord);
		List<ThresholdRecord> thresholdRecord = getThreshold(requestRecord);
		List<StatusRecord> requesStatusList = new ArrayList<StatusRecord>();
		StatusRecord requestStatus = null;
		
		//for each ATP code build the reponse status record(s)
		for(StatusRequestRecord sr : requestRecord){
			//Step 1.)Build submission record data
			if(requestRecord.size() > 0){
				requestStatus = new StatusRecord();
				requestStatus.setAtpCode(sr.getAtpCode());
				requestStatus.setSystemUuId(sr.getSystemUuId());
				for(SchoolElectronicSubmissionRecord sesr: submissionRecords){
					if(submissionRecords.size() > 0){
						if(sesr.getAtpCode().equalsIgnoreCase(sr.getAtpCode())){
							requestStatus.getFileStatus().add(sesr);
						}
					}
				}
				
			}
			//Step 2.) Build Batch Queue record data
			for(BatchProcessQueueRecord bpqr : queueRecords){
				if(queueRecords.size() > 0){
					if(bpqr.getAtpCode().equalsIgnoreCase(sr.getAtpCode())){
						requestStatus.setBatchType(bpqr.getRequester());
						requestStatus.setBatchStatus(bpqr.getJobStatus());
						//Step 2-a.) Build the Batch Process record data
						if(bpqr.getBatchStatus()!= null && bpqr.getBatchStatus().size() > 0 ){
							requestStatus.setBatchStep(bpqr.getBatchStatus().get(bpqr.getBatchStatus().size() - 1).getProcessName());
						}
					}
				}
			}
			//Step 3.) Build Threshold record data
			for(ThresholdRecord tr: thresholdRecord){
				
				if(tr.getSchoolAtpCode() != null){
					if(tr.getSchoolAtpCode().equalsIgnoreCase(sr.getAtpCode())){
						requestStatus.setThreshold(tr.getMatchingThresholdSymbolic());
					}
				}
			}
			
			requesStatusList.add(requestStatus);
		}
		
		return requesStatusList;
	}
	
	private List<BatchProcessStatusRecord> getBatchProcessStatus(int jobId){
		List<BatchProcessStatusRecord> result = new ArrayList<>();
		
		try (Connection conn = super.getConnection();
			PreparedStatement stmt = conn.prepareStatement(STATUS_QRY);) {
		
			stmt.setInt(1 , jobId);

			try (ResultSet rs = stmt.executeQuery();) {
				while (rs.next()) {
					BatchProcessStatusRecord entity = new BatchProcessStatusRecord();
					
					entity.setEndTime(rs.getDate("EndTime"));
					entity.setErrorRecordCount(rs.getInt("ErrorRecordCount"));
					entity.setJobId(rs.getInt("JobId"));
					entity.setProcessName(rs.getString("ProcessName"));
					entity.setProcessStatusId(rs.getInt("ProcessStatusID"));
					entity.setStartTime(rs.getDate("StartTime"));
					entity.setStatus(rs.getString("Status"));
					entity.setSuccessRecordCount(rs.getInt("SuccessRecordCount"));

					result.add(entity);
				}
			}
		} 
		catch (Exception e) {
			String msg = "SchoolDaoImpl.getBatchProcessStatus() SQL Exception in RowMapper: " + e.getMessage();
					
			logger.error(msg, e);
		} 
		
		return result;
	}
	
	@Override
	public List<BatchProcessQueueRecord> getAccessDbStatus(String[] aptCode) {
		List<BatchProcessQueueRecord> result = new ArrayList<>();
		
		this.inClauseBuilder(aptCode);
		
		String sqlQuery = new StringBuilder()
				.append("SELECT JobId, schoolATPCode, JobStatus, Requester, RecordCount, UploadTime, BatchCompletionTime, Submission_UUID")
				.append(" FROM BatchProcessQueue")
				.append(" WHERE schoolATPCode IN (")
				.append(createInClauseParams(aptCodeParams))
				.append(")")
				.append(" AND requester = 'AccessDB'")
				.toString();
		
		logger.debug(String.format("Executing prepared SQL statement [%s]", sqlQuery));
		
		try (Connection conn = super.getConnection();
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);) {
		
			setStatementParameters(stmt, aptCodeParams);
			
			try (ResultSet rs = stmt.executeQuery();) {
				while (rs.next()) {
					BatchProcessQueueRecord entity = new BatchProcessQueueRecord();
					
					entity.setAtpCode(rs.getString("schoolATPCode"));
					entity.setBatchCompletionTime(rs.getDate("BatchCompletionTime"));
					entity.setJobId(rs.getInt("JobId"));
					entity.setJobStatus(rs.getString("JobStatus"));
					entity.setRecordCount(rs.getInt("RecordCount"));
					entity.setRequester(rs.getString("Requester"));
					entity.setUpLoadTime(rs.getDate("UploadTime"));
					entity.setSystemUuId(rs.getString("Submission_UUID"));

					result.add(entity);
				}
			}
		} 
		catch (Exception e) {
			String msg = "SchoolDaoImpl.getAccessDbStatus() SQL Exception in RowMapper: " + e.getMessage();
					
			logger.error(msg, e);
		} 
		
		return result;
	}
	
	@Override
	public List<SchoolElectronicSubmissionRecord> createSchoolSubmissionRecord(UploadFileValidatorMessageDTO dto, String status) {
		if( ! isValidAtpCode(dto.getAtpCode()) ){
			return new ArrayList<SchoolElectronicSubmissionRecord>();
		}
		
		List<SchoolElectronicSubmissionRecord> result = new ArrayList<SchoolElectronicSubmissionRecord>();
		
		String sql = "{call excelSchoolElectronicSubmissionsInsert(?,?,?,?)}";
		
		logger.debug(String.format("Executing callable SQL statement excelSchoolElectronicSubmissionsInsert(%s, %s, %s, %s)", 
			     dto.getAtpCode(), dto.getExcelFileName(), dto.getRequestor(), dto.getUserName()));
		
		try (Connection conn = super.getConnection();
			 CallableStatement stmt = conn.prepareCall(sql);) {
					
			stmt.setString(1, dto.getAtpCode());
			stmt.setString(2, dto.getExcelFileName());
			stmt.setString(3, dto.getRequestor());
			stmt.setString(4, dto.getUserName());
				
			try (ResultSet rs = stmt.executeQuery();) {
				while(rs.next()) {
					SchoolElectronicSubmissionRecord entity = new SchoolElectronicSubmissionRecord();
					entity.setAutoElectronicSubmissionsID(rs.getInt("autoElectronicSubmissionsID"));
					entity.setSystemUuId(rs.getString("Submission_UUID"));
						
					result.add(entity);
	    		}
			}
		} 
		catch (Exception e) {
			logger.error("SchoolDaoImpl.createSchoolSubmissionRecord() - Exception while Inserting School Submission Record", e);
		}
		
		return result;
	}
	
	//@Override
	public List<SchoolElectronicSubmissionRecord> createSchoolSubmissionRecord_v2(UploadFileValidatorMessageDTO dto, String status) {
		if( ! isValidAtpCode(dto.getAtpCode()) ){
			return new ArrayList<SchoolElectronicSubmissionRecord>();
		}
		
		List<SchoolElectronicSubmissionRecord> result = new ArrayList<SchoolElectronicSubmissionRecord>();
		
		String sql = "{call excelSchoolElectronicSubmissionsInsert(?,?,?,?)}";
		
		System.out.println("Executing callable SQL statement [call excelSchoolElectronicSubmissionsInsert(?,?,?,?)]");
		
		System.out.println(
				String.format(">>> Executing2: excelSchoolElectronicSubmissionsInsert with params [%s, %s, %s, %s]", 
			    dto.getAtpCode(), dto.getExcelFileName(), dto.getRequestor(), dto.getUserName()));
		
		 try (Connection conn = super.getConnection();
		      CallableStatement stmt = conn.prepareCall(sql);) {
			 
			 stmt.setString(1, dto.getAtpCode());
			 stmt.setString(2, dto.getExcelFileName());
			 stmt.setString(3, dto.getRequestor());
			 stmt.setString(4, dto.getUserName());
			 
			boolean hadResults = stmt.execute();
			
			if(hadResults) {
				System.out.println(">>> Found results in SP");
				ResultSet rs = stmt.getResultSet();
				  
				while(rs.next()) {
					  SchoolElectronicSubmissionRecord entity = new SchoolElectronicSubmissionRecord();
					  entity.setAutoElectronicSubmissionsID(rs.getInt("autoElectronicSubmissionsID"));
					  entity.setSystemUuId(rs.getString("Submission_UUID"));
				  
					  result.add(entity);
				}
			}
			else {
				System.out.println(">>> No results found in SP");
			} 
		 }
		 catch (Exception e) {
			 String msg = "SchoolDaoImpl.createSchoolSubmissionRecord() - Exception while Inserting School Submission Record";
			 logger.error(msg, e);
			  
			 System.out.println(msg);
			 e.printStackTrace();
		 } 
		
		 return result;
	}
	
	//@Override
	public List<SchoolElectronicSubmissionRecord> createSchoolSubmissionRecord_v3(UploadFileValidatorMessageDTO dto, String status) {
		if( ! isValidAtpCode(dto.getAtpCode()) ){
			return new ArrayList<SchoolElectronicSubmissionRecord>();
		}
		
		List<SchoolElectronicSubmissionRecord> result = new ArrayList<SchoolElectronicSubmissionRecord>();
		
		String sql = "{call TES_Current.dbo.excelSchoolElectronicSubmissionsInsert(?,?,?,?,?,?)}";
		
		try (Connection conn = super.getConnection();
			 CallableStatement stmt = conn.prepareCall(sql);) {
			 
			stmt.setString(1, dto.getAtpCode());
			stmt.setString(2, dto.getExcelFileName());
			stmt.setString(3, dto.getRequestor());
			stmt.setString(4, dto.getUserName());
			stmt.registerOutParameter(5, java.sql.Types.INTEGER);
			stmt.registerOutParameter(6, java.sql.Types.VARCHAR);
			 
			stmt.executeUpdate();
			
			SchoolElectronicSubmissionRecord entity = new SchoolElectronicSubmissionRecord();
			entity.setAutoElectronicSubmissionsID(stmt.getInt(5));
			entity.setSystemUuId(stmt.getString(6));
			
			result.add(entity); 
		}
		catch (Exception e) {
			String msg = "SchoolDaoImpl.createSchoolSubmissionRecord() - Exception while Inserting School Submission Record";
			logger.error(msg, e);
		} 
		
		return result;
	}
	
	private boolean isValidAtpCode(String atpCode){
		List<ThresholdRecord> result = new ArrayList<>();
		
		logger.debug(String.format("Executing prepared SQL statement [%s]", VALID_ATP_CODE));
		
		try (Connection conn = super.getConnection();
			PreparedStatement stmt = conn.prepareStatement(VALID_ATP_CODE);) {
		
			logger.debug(String.format("Setting SQL statement parameter value: index %d, parameter %s", 1, atpCode));
			
			stmt.setString(1, atpCode);

			try (ResultSet rs = stmt.executeQuery();) {
				while (rs.next()) {
					ThresholdRecord entity = new ThresholdRecord();
					
					entity.setMatchingThresholdNumeric(0);
					entity.setMatchingThresholdSymbolic(0 + "%");
					entity.setSchoolAtpCode(rs.getString("schoolATPcode"));

					result.add(entity);
				}
			}
		} 
		catch (Exception e) {
			String msg = "SchoolDaoImpl.isValidAtpCode() SQL Exception in RowMapper: " + e.getMessage();
					
			logger.error(msg, e);
		}
		
		boolean retVal = false;
		
		if(result.size() > 0) {
			if(result.get(0).getSchoolAtpCode() != null && !result.get(0).getSchoolAtpCode().isEmpty()) {
				retVal = true;
			} else {
				retVal = false;
			}
		} else {
			retVal = false;
		}
		
		return retVal;
	}
	
	private void inClauseBuilder(List<StatusRequestRecord> requestRecord){
		aptCodeParams = new ArrayList<String>();
		uuIdParams = new ArrayList<String>();
		
		for(StatusRequestRecord srr: requestRecord){
			aptCodeParams.add(srr.getAtpCode());
			uuIdParams.add(srr.getSystemUuId());
		}
	}
	
	private void inClauseBuilder(String[] atpCode){
		aptCodeParams = new ArrayList<String>();
		for(String srr: atpCode){
			aptCodeParams.add(srr.toString());
		
		}
	}
	
	private String createInClauseParams(List<String> values) {
		List<String> inClauseParams = new ArrayList<>();
		
		for(int i = 0; i < values.size(); i++) {
			inClauseParams.add("?");
		}
		
		String commaSepInClauseParams = String.join(",", inClauseParams);
		
		return commaSepInClauseParams;
	}
	
	private void setStatementParameters(PreparedStatement stmt, List<String> parameters) throws SQLException {
		 int index = 1; 
		 for( String parameter : parameters ) { 
			 logger.debug(String.format("Setting SQL statement parameter value: index %d, parameter %s", index, parameter));
			 
			 stmt.setString(index++, parameter); 
		 }
	}
	
}