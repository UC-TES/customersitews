package com.ucop.tes.customersite.repository.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.ucop.tes.customersite.domain.PollerQueueDto;
import com.ucop.tes.customersite.domain.PollerStatusDto;
import com.ucop.tes.customersite.repository.BatchtProcessStatusDao;

@Stateless
public class BatchtProcessStatusDaoImpl extends GenericDao implements BatchtProcessStatusDao{
	private static Logger logger = Logger.getLogger(BatchtProcessStatusDaoImpl.class);
	
	public BatchtProcessStatusDaoImpl() {}
	
	@Override
	public void cleanJobStatus(String atpCode) throws Exception {
		String sql_1 = "DELETE bps FROM BatchProcessQueue bpq LEFT JOIN BatchProcessStatus"
				+ " bps ON bps.jobid = bpq.jobid WHERE bpq.schoolatpcode = ?"
				+ " AND bpq.Requester = 'AccessDB'";
		
		logger.debug(String.format("Executing prepared SQL statement [%s]", sql_1));
		
		try (Connection conn = super.getConnection();
			PreparedStatement stmt = conn.prepareStatement(sql_1);) {
			
			logger.debug(String.format("Setting SQL statement parameter value: index %d, parameter %s", 1, atpCode));
			
			stmt.setString(1, atpCode);
					
			stmt.executeUpdate();
				
		} catch (Exception e) {
			String msg = "AccessDbDaoImpl.cleanJobStatus() deleting BatchProcessStatus";
				
			logger.error(msg, e);
			    
			throw new Exception(msg, e);
		}
		
		String sql_2 = "DELETE BatchProcessQueue WHERE schoolatpcode = ?"
						+ " AND Requester = 'AccessDB'";
		
		logger.debug(String.format("Executing prepared SQL statement [%s]", sql_2));
		
		try (Connection conn = super.getConnection();
			PreparedStatement stmt = conn.prepareStatement(sql_2);) {
			
			logger.debug(String.format("Setting SQL statement parameter value: index %d, parameter %s", 1, atpCode));
			
			stmt.setString(1, atpCode);
					
			stmt.executeUpdate();
				
		} catch (Exception e) {
			String msg = "AccessDbDaoImpl.cleanJobStatus() deleting BatchProcessQueue - Exception:";
				
			logger.error(msg, e);
			    
			throw new Exception(msg, e);
		}
	}

	@Override
	public PollerQueueDto insertJobStatus(String atpCode) throws Exception {
		String sql = "INSERT INTO BatchProcessQueue (schoolATPCode,JobStatus,Requester"
		           + ",RecordCount,UploadTime,BatchCompletionTime,Submission_UUID)"
		           + " VALUES (?,?,?,0,GETDATE(),null,null)";
		
		PollerQueueDto dto = new PollerQueueDto();
		dto.setSchoolATPCode(atpCode);
		dto.setJobStatus("Processing");
		dto.setRequester("AccessDB");
		
		logger.debug(String.format("Executing prepared SQL statement [%s]", sql));
		
		try (Connection conn = super.getConnection();
			PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
			
			logger.debug(String.format("Setting SQL statement parameter value: index %d, parameter %s", 1, dto.getSchoolATPCode()));
			logger.debug(String.format("Setting SQL statement parameter value: index %d, parameter %s", 2, dto.getJobStatus()));
			logger.debug(String.format("Setting SQL statement parameter value: index %d, parameter %s", 3, dto.getRequester()));
			
			stmt.setString(1, dto.getSchoolATPCode());
			stmt.setString(2, dto.getJobStatus());
			stmt.setString(3, dto.getRequester());
					
			int affectedRows = stmt.executeUpdate();
			
			if (affectedRows == 0) {
	            throw new Exception("Inserting job status failed, no rows affected.");
	        }
			
			try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					dto.setJobId(generatedKeys.getLong(1));
				}
		        else {
		        	throw new Exception("Creating job status failed, no ID obtained.");
		        }
			}
				
		} catch (Exception e) {
			String msg = "\"AccessDbDaoImpl.insertJobStatus() Exception:";
				
			logger.error(msg, e);
			    
			throw new Exception(msg, e);
		}
		
		return dto;
	}


	@Override
	public PollerStatusDto insertJobStep(PollerStatusDto dto) throws Exception {
		String sql = "INSERT INTO BatchProcessStatus (JobID,ProcessName,Status"
				+ ",StartTime) VALUES (?,?,?,GETDATE())";
		
		logger.debug(String.format("Executing prepared SQL statement [%s]", sql));
		
		try (Connection conn = super.getConnection();
			PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
			
			logger.debug(String.format("Setting SQL statement parameter value: index %d, parameter %s", 1, dto.getJobId()));
			logger.debug(String.format("Setting SQL statement parameter value: index %d, parameter %s", 2, dto.getProcessName()));
			logger.debug(String.format("Setting SQL statement parameter value: index %d, parameter %s", 3, dto.getStatus()));
			
			stmt.setLong(1, dto.getJobId());
			stmt.setString(2, dto.getProcessName());
			stmt.setString(3, dto.getStatus());
					
			int affectedRows = stmt.executeUpdate();
			
			if (affectedRows == 0) {
	            throw new Exception("Inserting job step failed, no rows affected.");
	        }
			
			try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					dto.setProcessStatusID(generatedKeys.getLong(1));
				}
		        else {
		        	throw new Exception("Creating job step failed, no ID obtained.");
		        }
			}
				
		} catch (Exception e) {
			String msg = "\"AccessDbDaoImpl.insertJobStep() Exception:";
				
			logger.error(msg, e);
			    
			throw new Exception(msg, e);
		}
		
		return dto;
	}
	
	@Override
	public void updateJobStatus(PollerQueueDto jobStatus) throws Exception {
		String sql = "UPDATE BatchProcessQueue SET JobStatus = ?, BatchCompletionTime = GETDATE() WHERE JobId = ?";
		
		logger.debug(String.format("Executing prepared SQL statement [%s]", sql));
		
		try (Connection conn = super.getConnection();
			PreparedStatement stmt = conn.prepareStatement(sql);) {
			
			logger.debug(String.format("Setting SQL statement parameter value: index %d, parameter %s", 1, jobStatus.getJobStatus()));
			logger.debug(String.format("Setting SQL statement parameter value: index %d, parameter %s", 2, jobStatus.getJobId()));
			
			stmt.setString(1, jobStatus.getJobStatus());
			stmt.setLong(2, jobStatus.getJobId());
					
			stmt.executeUpdate();
				
		} catch (Exception e) {
			String msg = "AccessDbDaoImpl.updateJobStatus() Exception";
				
			logger.error(msg, e);
			    
			throw new Exception(msg, e);
		}
	}

	@Override
	public void updateJobStep(PollerStatusDto dto) throws Exception {
		String sql = "UPDATE BatchProcessStatus SET Status = ?, EndTime = GETDATE() WHERE ProcessStatusID = ?";
		
		logger.debug(String.format("Executing prepared SQL statement [%s]", sql));
		
		try (Connection conn = super.getConnection();
			PreparedStatement stmt = conn.prepareStatement(sql);) {
			
			logger.debug(String.format("Setting SQL statement parameter value: index %d, parameter %s", 1, dto.getStatus()));
			logger.debug(String.format("Setting SQL statement parameter value: index %d, parameter %s", 2, dto.getProcessStatusID()));
			
			stmt.setString(1, dto.getStatus());
			stmt.setLong(2, dto.getProcessStatusID());
			
			stmt.executeUpdate();
				
		} catch (Exception e) {
			String msg = "AccessDbDaoImpl.updateJobStep() Exception";
				
			logger.error(msg, e);
			    
			throw new Exception(msg, e);
		} 
	}
}