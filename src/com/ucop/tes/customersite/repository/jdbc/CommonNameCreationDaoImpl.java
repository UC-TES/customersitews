package com.ucop.tes.customersite.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

import javax.ejb.Stateless;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ucop.tes.customersite.domain.CommonNameCreationDto;
import com.ucop.tes.customersite.domain.ExpandedCourseNames;
import com.ucop.tes.customersite.repository.CommonNameCreationDao;

@Stateless
public class CommonNameCreationDaoImpl extends GenericDao implements CommonNameCreationDao {
	private static Logger logger = Logger.getLogger(CommonNameCreationDaoImpl.class);
	
	public CommonNameCreationDaoImpl() {}

	@Override
	public List<CommonNameCreationDto> getCommonNameCreation() {
		List<CommonNameCreationDto> result = new ArrayList<CommonNameCreationDto>();
		
		String sql = "{call TESCncCourseListsSelect(?,?)}";
		
		logger.debug(
				String.format("Executing callable SQL statement TESCncCourseListsSelect(%s, %s)", "OUT", "OUT")
		);
		
		try (Connection conn = super.getConnection();
			 CallableStatement stmt = conn.prepareCall(sql);) {
			
			stmt.registerOutParameter(1,java.sql.Types.INTEGER);
			stmt.registerOutParameter(2,java.sql.Types.CHAR);
				
			try (ResultSet rs = stmt.executeQuery();) {
				while(rs.next()) {
					CommonNameCreationDto entity = new CommonNameCreationDto();
					entity.setSourceCourseName(rs.getString("sourceCourseName"));;
						
					result.add(entity);
	    		}
			}
		} 
		catch (Exception e) {
			logger.error("CommonNameCreationDaoImpl.getCommonNameCreation() - Exception while loading Common Course Names", e);
		}
		
		return result;
	}
	
	/*
	 * TODO Convert store proc to sql statement
	 */
	@Override
	public List<ExpandedCourseNames> getExpandedNames() {
		List<ExpandedCourseNames> result = new ArrayList<ExpandedCourseNames>();
		
		String sql = "{call expandedCourseNameSelect(?,?)}";
		
		logger.debug(
				String.format("Executing callable SQL statement expandedCourseNameSelect(%s, %s)", "OUT", "OUT")
		);
		
		try (Connection conn = super.getConnection();
			 CallableStatement stmt = conn.prepareCall(sql);) {
			
			stmt.registerOutParameter(1,java.sql.Types.INTEGER);
			stmt.registerOutParameter(2,java.sql.Types.CHAR);
				
			try (ResultSet rs = stmt.executeQuery();) {
				while(rs.next()) {
					ExpandedCourseNames entity = new ExpandedCourseNames();
					entity.setAbbrevCourseName(rs.getString("abbrevCourseName"));
					entity.setExpandCourseName(rs.getString("expandCourseName"));
						
					result.add(entity);
	    		}
			}
		} 
		catch (Exception e) {
			logger.error("CommonNameCreationDaoImpl.getExpandedNames() - Exception while loading Expaneded Names", e);
		}
		
		return result;
	}

	/*
	 * TODO Convert store proc to sql statement
	 */
	@Override
	public void createCommonName(String sourceName, String commonName) {
		String sql = "{call commonCourseNameInsert(?,?,?,?) }";
		
		logger.debug(
				String.format("Executing callable SQL statement commonCourseNameInsert(%s, %s, %s, %s)", 
				sourceName, commonName, "OUT", "OUT")
		);
		
		try (Connection conn = super.getConnection();
		     CallableStatement stmt = conn.prepareCall(sql);) {
		
			stmt.setString(1, sourceName);
			stmt.setString(2, commonName);
			stmt.registerOutParameter(3,java.sql.Types.INTEGER);
			stmt.registerOutParameter(4,java.sql.Types.CHAR);

			stmt.execute();

		} catch (Exception e) {
			String msg = String.format("CommonNameCreationDaoImpl.createCommonName() - Failed to save source name [%s] : common name [%s] to tablecommonCourseName" , sourceName, commonName);
			
			logger.error(msg, e);
		}
	}
	
}