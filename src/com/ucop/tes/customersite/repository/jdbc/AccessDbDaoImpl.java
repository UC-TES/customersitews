package com.ucop.tes.customersite.repository.jdbc;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.DatabaseBuilder;
import com.healthmarketscience.jackcess.util.ImportUtil;
import com.ucop.tes.customersite.domain.PollerQueueDto;
import com.ucop.tes.customersite.domain.PollerStatusDto;
import com.ucop.tes.customersite.repository.AccessDbDao;
import com.ucop.tes.customersite.repository.BatchtProcessStatusDao;
import com.ucop.tes.customersite.services.AwsService;

@Stateless
public class AccessDbDaoImpl extends GenericDao implements AccessDbDao {
	private static Logger logger = Logger.getLogger(AccessDbDaoImpl.class);
		
	@EJB
	private AwsService awsService;
	
	@EJB
	private BatchtProcessStatusDao batchService;
	
	private Database db;
	
	private Map<String,String> params;
	
	private static String sql;
	
	@Override
	public void createMSAccessDB(String[] atpCode) {
		
		File accessDbFile = null;
		PollerQueueDto jobStatus = null;
		
		try {
			for(int i = 0; i < atpCode.length; i++){
				this.batchService.cleanJobStatus(atpCode[i]);
				jobStatus = this.batchService.insertJobStatus(atpCode[i]);
				params = new HashMap<>();
				params.put("atpCode", atpCode[i]);
				accessDbFile = new File(atpCode[i] + ".mdb");
				db = DatabaseBuilder.create(Database.FileFormat.V2000, accessDbFile);
				createStudentTable(jobStatus);
				createSchoolTable(jobStatus);
				createSchoolAttendedTable(jobStatus);
				createStudentCourseTable(jobStatus);
				//TESR-486
				/*createStudentEthnicityTable(jobStatus);*/
				
				createStudentQualificationTable(jobStatus);
				createStudentTestTable(jobStatus);
				createTesSchoolTable(jobStatus);	
				db.close();
				
				PollerStatusDto statusDto = new PollerStatusDto();
				statusDto.setJobId(jobStatus.getJobId());
				statusDto.setProcessName(atpCode[i] + ".mdbToAWS S3");
				statusDto.setStartTime(new Date());
				statusDto.setStatus("Processing");
				statusDto = this.batchService.insertJobStep(statusDto);
				awsService.copyFileToAwsS3(accessDbFile, atpCode[i]);
				statusDto.setEndTime(new Date());
				statusDto.setStatus("Complete");
				this.batchService.updateJobStep(statusDto);
				
				
				jobStatus.setJobStatus("Complete");
				jobStatus.setBatchCompletionTime(new Date());
				this.batchService.updateJobStatus(jobStatus);
			}
			
		} catch (IOException e) {
			logger.error("AccessDbDaoImpl.createMSAccessDB() IOException creating MDB file: " + e.getMessage(),e);
			jobStatus.setJobStatus("ERROR:MDB Create");
			jobStatus.setBatchCompletionTime(new Date());
			try {
				this.batchService.updateJobStatus(jobStatus);
			} catch (Exception e1) {
				
			}
			e.printStackTrace();
		} catch (Exception e){
			logger.error("AccessDbDaoImpl.createMSAccessDB() Exception creating MDB file: " + e.getMessage(),e);
			jobStatus.setJobStatus("ERROR:MDB Create");
			jobStatus.setBatchCompletionTime(new Date());
			try {
				this.batchService.updateJobStatus(jobStatus);
			} catch (Exception e1) {
				
			}
			e.printStackTrace();
		}
		
	}
	
	private void createStudentTable(PollerQueueDto jobStatus) throws Exception {
		/*sql = "SELECT st.studentID,st.schoolATPCode,st.schoolMagnetCode,st.gradeLevel,st.studentNeedsAttentionFlag,"
				+ "st.studentNeedsReviewFlag,st.studentTier,st.globalCourseChangeFlag,st.doorwaysCourseChangeFlag,"
				+ "st.localStudentID,st.CAStateStudentID,st.IDCode,st.studentFName,st.studentMName,"
				+ "st.studentLName,st.address1,st.address2,st.city,st.state,st.zip,st.phone,"
				+ "st.birthDate,st.gender,st.hsGraduationDate,st.localethnicity,st.cbedsethnicity,"
				+ "st.schoolClassSize,st.schoolClassRank,st.studentRank,st.schoolGPA,st.level1EvaluatorWebUserID,"
				+ "st.level1EvaluatorStudentStatusCode,st.level1EvaluatorStatusDate,st.level2EvaluatorWebUserID,"
				+ "st.level2EvaluatorStudentStatusCode,st.level2EvaluatorStatusDate,st.lunchProgramCode,"
				+ "st.studentEvaluationResultsChangedFlag,st.confidenceScoreValue,st.studentConfidenceCodes,"
				+ "st.calGrantReleaseFormReceivedFlag,st.schoolElectronicSubmissionID,st.programName,"
				+ "st.UCApplicationID,st.CSUApplicationID,st.GPAType,st.languageCode,st.LOTECertSource,"
				+ "st.transcriptNote,st.confidenceScoreNote,st.level1EvaluatorNote,st.level2EvaluatorNote,"
				+ "st.supportNote FROM dbo.student st WHERE st.schoolATPCode = :atpCode"; */
		
		// TESR-486
		sql = "SELECT [studentID],[schoolATPCode],[gradeLevel],[localStudentID],[CAStateStudentID],[studentFName],[studentLName]"
			+ ",[address1],[city],[state],[zip],[birthDate],[gender],[hsGraduationDate],[localethnicity],[lunchProgramCode]"
			+ ",[programName] FROM [dbo].[student]  WHERE schoolATPCode = ?";
		
		PollerStatusDto statusDto = new PollerStatusDto();
		statusDto.setJobId(jobStatus.getJobId());
		statusDto.setProcessName("CreateStudentTable");
		statusDto.setStartTime(new Date());
		statusDto.setStatus("Processing");
		statusDto = this.batchService.insertJobStep(statusDto);
		
		logger.debug(String.format("Executing prepared SQL statement [%s]", sql));
		
		try (Connection conn = super.getConnection();
			 PreparedStatement stmt = conn.prepareStatement(sql);) {
			
			 stmt.setString(1 , params.values().iterator().next()); 
			
			 try (ResultSet rs = stmt.executeQuery();) {
				 new ImportUtil.Builder(db, "student").importResultSet(rs);
			}
			
		}
		catch (SQLException | IOException e) {
			String msg = "AccessDbDaoImpl.createStudentTable() Exception";
					
			logger.error(msg, e);
					
			jobStatus.setJobStatus("ERROR:CreateStudent");
			jobStatus.setBatchCompletionTime(new Date());
			this.batchService.updateJobStatus(jobStatus);
					
			throw new Exception(msg, e);
		}
		
		statusDto.setEndTime(new Date());
		statusDto.setStatus("Complete");
		this.batchService.updateJobStep(statusDto);
	}
	
	private void createSchoolTable(PollerQueueDto jobStatus) throws Exception{
		/*
		sql = "SELECT s.schoolATPCode,s.districtID,s.publicSchoolFlag,s.name,s.mailingAddress,"
				+ "s.mailingCity,s.mailingState,s.mailingZip,s.streetAddress,s.streetCity,s.streetState," 
				+ "s.streetZip,s.phone,s.fax,s.CDSCode,s.targetSchoolFlag,s.charterSchoolFlag,s.smallCommunityFlag,"
				+ "s.catholicSchoolFlag,s.expectedSubmissionSource,s.sisType,s.originalSchoolATPCode," 
				+ "s.originalSchoolATPCodeDateChange FROM dbo.school s WHERE s.schoolATPCode = :atpCode";*/
		
		// TESR-486
		sql = "SELECT s.schoolATPCode,s.districtID,s.publicSchoolFlag,s.name,s.mailingAddress,s.mailingCity,"
				+ "s.mailingState,s.mailingZip,s.streetAddress,s.streetCity,s.streetState,s.streetZip,s.phone,"
				+ "s.fax,s.CDSCode,s.charterSchoolFlag FROM dbo.school s WHERE s.schoolATPCode = ?";
		
		
		PollerStatusDto statusDto = new PollerStatusDto();
		statusDto.setJobId(jobStatus.getJobId());
		statusDto.setProcessName("CreateSchoolTable");
		statusDto.setStartTime(new Date());
		statusDto.setStatus("Processing");
		statusDto = this.batchService.insertJobStep(statusDto);
		
		logger.debug(String.format("Executing prepared SQL statement [%s]", sql));
		
		try (Connection conn = super.getConnection();
			 PreparedStatement stmt = conn.prepareStatement(sql);) {
			
			stmt.setString(1 , params.values().iterator().next()); 
			
			 try (ResultSet rs = stmt.executeQuery();) {
				 new ImportUtil.Builder(db, "school").importResultSet(rs);
			}
			
		}
		catch (SQLException | IOException e) {
			String msg = "AccessDbDaoImpl.createSchoolTable() Exception";
			
			logger.error(msg, e);
						
			jobStatus.setJobStatus("ERROR:CreateSchool");
			jobStatus.setBatchCompletionTime(new Date());
			this.batchService.updateJobStatus(jobStatus);
						
			throw new Exception(msg, e);
		}
		
		statusDto.setEndTime(new Date());
		statusDto.setStatus("Complete");
		
		this.batchService.updateJobStep(statusDto);
	}
	
	private void createSchoolAttendedTable(PollerQueueDto jobStatus) throws Exception{
		sql = "SELECT sa.schoolAttendedID,sa.schoolATPCode,sa.submittedName,sa.submittedATPCode," 
				+ "sa.submittedCDSCode,sa.schoolSourceCode,sa.nonSourceSchoolID,sa.schoolTypeID,sa.name,"
				+ "sa.city,sa.district,sa.state,sa.countryCode,sa.ATPCode,sa.CDSCode,sa.schoolMatchedStatusFlag,"
				+ "sa.schoolMatchedModifiedFlag,sa.transcriptMatchedStatusCode,[countryName] = ccl.Name,"
				+ " [schoolTypeDesc] = stl.Description FROM dbo.schoolAttended sa INNER JOIN dbo.CountryCodeLookup ccl"
				+ " ON sa.countryCode = ccl.CountryCode INNER JOIN dbo.SchoolTypeLookup stl"
				+ " ON sa.schoolTypeID = stl.schoolTypeID WHERE sa.schoolATPCode = ?";
		
		PollerStatusDto statusDto = new PollerStatusDto();
		statusDto.setJobId(jobStatus.getJobId());
		statusDto.setProcessName("CreateSchoolAttendTable");
		statusDto.setStartTime(new Date());
		statusDto.setStatus("Processing");
		statusDto = this.batchService.insertJobStep(statusDto);
		
		logger.debug(String.format("Executing prepared SQL statement [%s]", sql));
		
		try (Connection conn = super.getConnection();
			 PreparedStatement stmt = conn.prepareStatement(sql);) {
			
			stmt.setString(1 , params.values().iterator().next()); 
			
			 try (ResultSet rs = stmt.executeQuery();) {
				 new ImportUtil.Builder(db, "schoolAttended").importResultSet(rs);
			}
			
		}
		catch (SQLException | IOException e) {
			String msg = "AccessDbDaoImpl.createSchoolAttendedTable() Exception";
			
			logger.error(msg, e);
						
			jobStatus.setJobStatus("ERROR:CreateAttended");
			jobStatus.setBatchCompletionTime(new Date());
			this.batchService.updateJobStatus(jobStatus);
						
			throw new Exception(msg, e);
		}
		
		statusDto.setEndTime(new Date());
		statusDto.setStatus("Complete");
		this.batchService.updateJobStep(statusDto);
	}
	
	private void createStudentCourseTable(PollerQueueDto jobStatus) throws Exception{
		/*
		sql = "SELECT sc.autoStudentCourseID,sc.studentID,sc.schoolAttendedID,sc.academicYearID,"
				+ " sc.otherCourseFlag,sc.gradeLevel,sc.calendarType,sc.term,sc.creditsAttempted,"
				+ " sc.creditsEarned,sc.workInProgressFlag,sc.excludeFromEvaluationFlag,sc.letterGrade,"
				+ " sc.letterGradeVerbatim,sc.blockSchedulingFlag,sc.transcriptCourseID,"
				+ " sc.transcriptCourseName,sc.courseID,sc.courseListTypeID,sc.categoryID,sc.languageKey,"
				+ " sc.courseName,sc.honorsKey,sc.semesterOnly,sc.courseInformationModifiedFlag,"
				+ " sc.usedInUCQualificationFlag,sc.usedAsUCElectiveFlag,sc.usedInUCWIPQualificationFlag,"
				+ " sc.usedAsUCWIPElectiveFlag,sc.usedInCSUQualificationFlag,sc.usedAsCSUElectiveFlag,"
				+ " sc.usedInCSUWIPQualificationFlag,sc.usedAsCSUWIPElectiveFlag,sc.usedInBasicAGQualificationFlag,"
				+ " sc.usedAsBasicAGElectiveFlag,sc.usedInBasicAGWIPQualificationFlag,"
				+ " sc.usedAsBasicAGWIPElectiveFlag,sc.UCOrphanQuarter,sc.CSUOrphanQuarter,sc.BasicAGOrphanQuarter,"
				+ " sc.addedByWebUserID,sc.repeatedCodeUC,sc.repeatedCodeUCWIP,sc.repeatedCodeCSU,"
				+ " sc.repeatedCodeCSUWIP,sc.repeatedCodeBasicAG,sc.repeatedCodeBasicAGWIP,sc.gradeValidatedFlagUC,"
				+ " sc.gradeValidatedFlagUCWIP,sc.gradeValidatedFlagCSU,sc.gradeValidatedFlagCSUWIP,"
				+ " sc.gradeValidatedFlagBasicAG,sc.gradeValidatedFlagBasicAGWIP,sc.collegeCourseEligibleFlag,"
				+ " sc.altTranscriptCourseName1,sc.altTranscriptCourseName2,sc.altTranscriptCourseName3,"
				+ " sc.academicIndicator,sc.collegePrepIndicator,sc.courseNote,sc.systemNote,sc.level1EvaluatorNote,"
				+ " sc.level2EvaluatorNote,sc.globalCourseChangeNote FROM dbo.studentCourse sc "
				+ " INNER JOIN dbo.student st ON sc.StudentID = st.StudentID WHERE st.schoolATPCode = :atpCode"; */
		
		// TESR-486
		sql = "SELECT sc.autoStudentCourseID,sc.studentID,sc.schoolAttendedID,sc.academicYear, sc.gradeLevel,sc.calendarType,"
				+ "sc.term,sc.creditsAttempted,sc.creditsEarned,sc.workInProgressFlag,sc.letterGrade,sc.letterGradeVerbatim,"
				+ "sc.blockSchedulingFlag,sc.transcriptCourseID,sc.transcriptCourseName,sc.courseID,sc.courseListTypeID,"
				+ "sc.categoryID,sc.languageKey,sc.courseName,sc.honorsKey,sc.semesterOnly,sc.courseInformationModifiedFlag"
				+ " FROM dbo.studentCourse sc INNER JOIN dbo.student st ON sc.StudentID = st.StudentID WHERE st.schoolATPCode = ?";
		
		PollerStatusDto statusDto = new PollerStatusDto();
		statusDto.setJobId(jobStatus.getJobId());
		statusDto.setProcessName("CreateStudentCourseTable");
		statusDto.setStartTime(new Date());
		statusDto.setStatus("Processing");
		statusDto = this.batchService.insertJobStep(statusDto);
		
		logger.debug(String.format("Executing prepared SQL statement [%s]", sql));
		
		try (Connection conn = super.getConnection();
			 PreparedStatement stmt = conn.prepareStatement(sql);) {
			
			stmt.setString(1 , params.values().iterator().next()); 
			
			 try (ResultSet rs = stmt.executeQuery();) {
				 new ImportUtil.Builder(db, "studentCourse").importResultSet(rs);
			}
			
		}
		catch (SQLException | IOException e) {
			String msg = "AccessDbDaoImpl.createStudentCourseTable() Exception";
			
			logger.error(msg, e);
							
			jobStatus.setJobStatus("ERROR:CreateCourse");
			jobStatus.setBatchCompletionTime(new Date());
			this.batchService.updateJobStatus(jobStatus);
							
			throw new Exception(msg, e);
		}
		
		statusDto.setEndTime(new Date());
		statusDto.setStatus("Complete");
		this.batchService.updateJobStep(statusDto);
	}
	
	//TESR-486
	/*private void createStudentEthnicityTable(PollerQueueDto jobStatus) throws Exception{
		ResultSet rs = null;
		
		sql = "SELECT se.studentID,se.hispanicEthnicity,se.raceCode1,	se.raceCode2,se.raceCode3,"
				+ " se.raceCode4,se.raceCode5 FROM dbo.studentEthnicity se INNER JOIN dbo.student st"
				+ " ON se.StudentID = st.StudentID WHERE st.schoolATPCode = :atpCode";
		
		try {
			PollerStatusDto statusDto = new PollerStatusDto();
			statusDto.setJobId(jobStatus.getJobId());
			statusDto.setProcessName("CreateEthnicityTable");
			statusDto.setStartTime(new Date());
			statusDto.setStatus("Processing");
			statusDto = this.batchService.insertJobStep(statusDto);
			
			rs = ((ResultSetWrappingSqlRowSet)jdbcTemplate.queryForRowSet(sql, params)).getResultSet();
			new ImportUtil.Builder(db, "studentEthnicity").importResultSet(rs);
			rs.close();
			
			statusDto.setEndTime(new Date());
			statusDto.setStatus("Complete");
			this.batchService.updateJobStep(statusDto);
			
		} catch (SQLException | IOException e) {
			logger.error("AccessDbDaoImpl.createStudentEthnicityTable() Exception: " + e.getMessage(),e);
			jobStatus.setJobStatus("ERROR:CreateEthnicty");
			jobStatus.setBatchCompletionTime(new Date());
			this.batchService.updateJobStatus(jobStatus);
			e.printStackTrace();
		}
	}*/
	
	private void createStudentQualificationTable(PollerQueueDto jobStatus) throws Exception {
		/*sql = "SELECT sq.studentID,sq.calGrantGPA,sq.BAGSubjectOnTrackCode,sq.BAGGPA,sq.BAGCategoryUsedInDC1,"
				+ " sq.BAGCategoryUsedInDC2,sq.BAGWIPSubjectOnTrackCode,sq.BAGWIPCategoryUsedInDC1,"
				+ " sq.BAGWIPCategoryUsedInDC2,sq.UCSubjectOnTrackCode,sq.UCGPA,sq.UC7of15Flag,sq.UCCategoryUsedInD1,"
				+ " sq.UCCategoryUsedInD2,sq.UCWIPSubjectOnTrackCode,sq.UCWIP7of15Flag,sq.UCWIPCategoryUsedInD1,"
				+ " sq.UCWIPCategoryUsedInD2,sq.CSUSubjectOnTrackCode,sq.CSUGPA,sq.CSUCategoryUsedInDC1,"
				+ " sq.CSUCategoryUsedInDC2,sq.CSUWIPSubjectOnTrackCode,sq.CSUWIPCategoryUsedInDC1,"
				+ " sq.CSUWIPCategoryUsedInDC2,sq.UCMissingAUS,sq.UCMissingAWGC,sq.UCMissingB,sq.UCMissingCAlg1,"
				+ " sq.UCMissingCAlg2,sq.UCMissingCGeo,sq.UCMissingD1,sq.UCMissingD2,sq.UCMissingE1,sq.UCMissingE2,"
				+ " sq.UCMissingF,sq.UCMissingG,sq.UCWIPMissingAUS,sq.UCWIPMissingAWGC,sq.UCWIPMissingB,"
				+ " sq.UCWIPMissingCAlg1,sq.UCWIPMissingCAlg2,sq.UCWIPMissingCGeo,sq.UCWIPMissingD1,sq.UCWIPMissingD2,"
				+ " sq.UCWIPMissingE1,sq.UCWIPMissingE2,sq.UCWIPMissingF,sq.UCWIPMissingG,sq.UCTotalAUS,"
				+ " sq.UCTotalAWGC,sq.UCTotalB,sq.UCTotalCAlg1,sq.UCTotalCGeo,sq.UCTotalCAlg2,sq.UCTotalCAdvMath,"	
				+ " sq.UCTotalDBio,sq.UCTotalDChem,sq.UCTotalDPhy,sq.UCTotalDInt,sq.UCTotalE1,sq.UCTotalE2,"
				+ " sq.UCTotalE3,sq.UCTotalE4,sq.UCTotalF,sq.UCTotalG,sq.UCWIPTotalAUS,sq.UCWIPTotalAWGC,"
				+ " sq.UCWIPTotalB,sq.UCWIPTotalCAlg1,sq.UCWIPTotalCGeo,sq.UCWIPTotalCAlg2,sq.UCWIPTotalCAdvMath,"
				+ " sq.UCWIPTotalDBio,sq.UCWIPTotalDChem,sq.UCWIPTotalDPhy,sq.UCWIPTotalDInt,sq.UCWIPTotalE1,"
				+ " sq.UCWIPTotalE2,sq.UCWIPTotalE3,sq.UCWIPTotalE4,sq.UCWIPTotalF,sq.UCWIPTotalG,sq.CSUMissingA,"
				+ " sq.CSUMissingAC,sq.CSUMissingB,sq.CSUMissingCAlg1,sq.CSUMissingCGeo,sq.CSUMissingCAlg2,"
				+ " sq.CSUMissingDC1,sq.CSUMissingDC2,sq.CSUMissingE1,sq.CSUMissingE2,sq.CSUMissingF,sq.CSUMissingG,"
				+ " sq.CSUWIPMissingA,sq.CSUWIPMissingAC,sq.CSUWIPMissingB,sq.CSUWIPMissingCAlg1,sq.CSUWIPMissingCGeo,"
				+ " sq.CSUWIPMissingCAlg2,sq.CSUWIPMissingDC1,sq.CSUWIPMissingDC2,sq.CSUWIPMissingE1,sq.CSUWIPMissingE2,"
				+ " sq.CSUWIPMissingF,sq.CSUWIPMissingG,sq.CSUTotalA,sq.CSUTotalAC,sq.CSUTotalB,sq.CSUTotalCAlg1,"
				+ " sq.CSUTotalCGeo,sq.CSUTotalCAlg2,sq.CSUTotalCAdvMath,sq.CSUTotalDCBio,sq.CSUTotalDCPhy,"
				+ " sq.CSUTotalDCInt,sq.CSUTotalE1,sq.CSUTotalE2,sq.CSUTotalE3,sq.CSUTotalE4,sq.CSUTotalF,"
				+ " sq.CSUTotalG,sq.CSUWIPTotalA,sq.CSUWIPTotalAC,sq.CSUWIPTotalB,sq.CSUWIPTotalCAlg1,"
				+ " sq.CSUWIPTotalCGeo,sq.CSUWIPTotalCAlg2,sq.CSUWIPTotalCAdvMath,sq.CSUWIPTotalDCBio,sq.CSUWIPTotalDCPhy,"
				+ " sq.CSUWIPTotalDCInt,sq.CSUWIPTotalE1,sq.CSUWIPTotalE2,sq.CSUWIPTotalE3,sq.CSUWIPTotalE4,"
				+ " sq.CSUWIPTotalF,sq.CSUWIPTotalG,sq.BAGMissingA,sq.BAGMissingAC,sq.BAGMissingB,sq.BAGMissingCAlg1,"
				+ " sq.BAGMissingCGeo,sq.BAGMissingCAlg2,sq.BAGMissingDC1,sq.BAGMissingDC2,sq.BAGMissingE1,"
				+ " sq.BAGMissingE2,sq.BAGMissingF,sq.BAGMissingG,sq.BAGWIPMissingA,sq.BAGWIPMissingAC,sq.BAGWIPMissingB,"
				+ " sq.BAGWIPMissingCAlg1,sq.BAGWIPMissingCGeo,sq.BAGWIPMissingCAlg2,sq.BAGWIPMissingDC1,"
				+ " sq.BAGWIPMissingDC2,sq.BAGWIPMissingE1,sq.BAGWIPMissingE2,sq.BAGWIPMissingF,sq.BAGWIPMissingG,"
				+ " sq.BAGTotalA,sq.BAGTotalAC,sq.BAGTotalB,sq.BAGTotalCGeo,sq.BAGTotalCAlg1,sq.BAGTotalCAlg2,"
				+ " sq.BAGTotalCAdvMath,sq.BAGTotalDCBio,sq.BAGTotalDCPhy,sq.BAGTotalDCInt,sq.BAGTotalE1,"
				+ " sq.BAGTotalE2,sq.BAGTotalE3,sq.BAGTotalE4,sq.BAGTotalF,sq.BAGTotalG,sq.BAGWIPTotalA,"
				+ " sq.BAGWIPTotalAC,sq.BAGWIPTotalB,sq.BAGWIPTotalCGeo,sq.BAGWIPTotalCAlg1,sq.BAGWIPTotalCAlg2,"
				+ " sq.BAGWIPTotalCAdvMath,sq.BAGWIPTotalDCBio,sq.BAGWIPTotalDCPhy,sq.BAGWIPTotalDCInt,"
				+ " sq.BAGWIPTotalE1,sq.BAGWIPTotalE2,sq.BAGWIPTotalE3,sq.BAGWIPTotalE4,sq.BAGWIPTotalF,sq.BAGWIPTotalG"
				+ " FROM dbo.studentQualification sq INNER JOIN dbo.student st ON sq.StudentID = "			
				+ " st.StudentID WHERE st.schoolATPCode = :atpCode";
		*/
		sql = " SELECT sqh.studentID ,sqh.SystemName ,sqh.AGLevel,sqh.Completed_Units ,sqh.Current_Units,sqh.Missing_Units "
				+ " ,sqh.Missing_WIP_Units ,sqh.SubjectBenchmarkStatus,sqh.AGCategory FROM TES_Reporting.dbo.StudentQualificationReporting as sqh "
				+ " join TES_Reporting.dbo.studentReporting as s on s.studentID = sqh.studentID WHERE s.SchoolATPCode = ?";
		
		PollerStatusDto statusDto = new PollerStatusDto();
		statusDto.setJobId(jobStatus.getJobId());
		statusDto.setProcessName("CreateStudentQualTable");
		statusDto.setStartTime(new Date());
		statusDto.setStatus("Processing");
		statusDto = this.batchService.insertJobStep(statusDto);
		
		logger.debug(String.format("Executing prepared SQL statement [%s]", sql));
		
		try (Connection conn = super.getConnection();
			 PreparedStatement stmt = conn.prepareStatement(sql);) {
			
			stmt.setString(1 , params.values().iterator().next()); 
			
			 try (ResultSet rs = stmt.executeQuery();) {
				 new ImportUtil.Builder(db, "studentQualification").importResultSet(rs);
			}
			
		}
		catch (SQLException | IOException e) {
			String msg = "AccessDbDaoImpl.createStudentQaulificationTable() Exception";
			
			logger.error(msg, e);
								
			jobStatus.setJobStatus("ERROR:CreateQual");
			jobStatus.setBatchCompletionTime(new Date());
			this.batchService.updateJobStatus(jobStatus);
								
			throw new Exception(msg, e);
		}
		
		statusDto.setEndTime(new Date());
		statusDto.setStatus("Complete");
		this.batchService.updateJobStep(statusDto);
	}
	
	private void createStudentTestTable(PollerQueueDto jobStatus) throws Exception{
		sql = "SELECT stt.autoStudentTestID,stt.studentID,stt.testID,stt.testScore,stt.testMonth,stt.testYear,"
				+ " stt.unknownTestName,stt.unknownTestSubject,stt.level1EvaluatorNote,stt.level2EvaluatorNote"
				+ " FROM dbo.studentTest stt INNER JOIN dbo.student st" 
				+ " ON stt.StudentID = st.StudentID WHERE st.schoolATPCode = ?";
		
		PollerStatusDto statusDto = new PollerStatusDto();
		statusDto.setJobId(jobStatus.getJobId());
		statusDto.setProcessName("CreateStudentTestTable");
		statusDto.setStartTime(new Date());
		statusDto.setStatus("Processing");
		statusDto = this.batchService.insertJobStep(statusDto);
		
		logger.debug(String.format("Executing prepared SQL statement [%s]", sql));
		
		try (Connection conn = super.getConnection();
			 PreparedStatement stmt = conn.prepareStatement(sql);) {
			
			stmt.setString(1 , params.values().iterator().next()); 
			
			 try (ResultSet rs = stmt.executeQuery();) {
				 new ImportUtil.Builder(db, "studentTest").importResultSet(rs);
			}
			
		}
		catch (SQLException | IOException e) {
			String msg = "AccessDbDaoImpl.createStudentTestTable() Exception";
			
			logger.error(msg, e);
									
			jobStatus.setJobStatus("ERROR:CreateTest");
			jobStatus.setBatchCompletionTime(new Date());
			this.batchService.updateJobStatus(jobStatus);
									
			throw new Exception(msg, e);
		}
		
		statusDto.setEndTime(new Date());
		statusDto.setStatus("Complete");
		this.batchService.updateJobStep(statusDto);
	}
	
	private void createTesSchoolTable(PollerQueueDto jobStatus) throws Exception{
		/*
		sql = "SELECT ts.schoolATPCode,ts.eligibleSchoolFlag,ts.statusDate,ts.initialLoadDate,"
				+ " ts.schoolConfidenceScore,ts.schoolMatcherID,ts.transcriptMatcherID,ts.level1EvaluatorID,"
				+ " ts.level2EvaluatorID,ts.carnegieUnit,ts.blockSchedulingFlag,ts.specialHandlingFlag,"
				+ " ts.schoolCalendarTypeCurrentYear,ts.schoolCalendarTypePreviousYear,ts.schoolCalendarTypePreviousYear2,"
				+ " ts.schoolCalendarTypePreviousYear3,ts.schoolLevel1StatusGrade09,ts.schoolLevel1StatusGrade10,"
				+ " ts.schoolLevel1StatusGrade11,ts.schoolLevel1StatusGrade12,ts.schoolLevel1StatusGrade13,"
				+ " ts.schoolLevel2StatusGrade09,ts.schoolLevel2StatusGrade10,ts.schoolLevel2StatusGrade11,"
				+ " ts.schoolLevel2StatusGrade12,ts.schoolLevel2StatusGrade13,ts.courseCount,ts.benchmarkTerm,"
				+ " ts.gradingSystem,ts.transcriptNote,ts.confidenceScoreNote,ts.schoolMatcherNote,"
				+ " ts.transcriptMatcherNote,ts.level1EvaluatorNote,ts.level2EvaluatorNote,ts.supportNote,"
				+ " ts.eligibleNotThisYearReason FROM dbo.tesSchool ts WHERE ts.schoolATPCode = :atpCode";*/
		
		//TESR-486
		sql = "SELECT schoolATPCode,statusID,statusDate,transcriptMatcherID,benchmarkTerm,transcriptMatcherNote"
				+ ",supportNote,threshold FROM dbo.tesSchool WHERE schoolATPCode = ?";
		
		PollerStatusDto statusDto = new PollerStatusDto();
		statusDto.setJobId(jobStatus.getJobId());
		statusDto.setProcessName("CreateTesSchoolTable");
		statusDto.setStartTime(new Date());
		statusDto.setStatus("Processing");
		statusDto = this.batchService.insertJobStep(statusDto);
		
		logger.debug(String.format("Executing prepared SQL statement [%s]", sql));
		
		try (Connection conn = super.getConnection();
			 PreparedStatement stmt = conn.prepareStatement(sql);) {
			
			stmt.setString(1 , params.values().iterator().next()); 
			
			 try (ResultSet rs = stmt.executeQuery();) {
				 new ImportUtil.Builder(db, "tesSchool").importResultSet(rs);
			}
			
		}
		catch (SQLException | IOException e) {
			String msg = "AccessDbDaoImpl.createTesSchoolTable() Exception";
			
			logger.error(msg, e);
										
			jobStatus.setJobStatus("ERROR:CreateTESSchl");
			jobStatus.setBatchCompletionTime(new Date());
			this.batchService.updateJobStatus(jobStatus);
										
			throw new Exception(msg, e);
		}
		
		statusDto.setEndTime(new Date());
		statusDto.setStatus("Complete");
		this.batchService.updateJobStep(statusDto);
	}
			
}