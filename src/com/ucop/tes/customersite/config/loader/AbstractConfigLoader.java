package com.ucop.tes.customersite.config.loader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public abstract class AbstractConfigLoader {
	private static final Logger logger = LogManager.getLogger(AbstractConfigLoader.class);
	
	public Properties loadProperties(String configFileName) {
		if(configFileName == null || configFileName.isEmpty())
			throw new IllegalArgumentException();
		
		logger.info("Initializing application config properties");
		
		Properties props = new Properties();

		try {
			File configFile = createConfigFile(configFileName);
			
			props.load(new FileInputStream(configFile));
		} catch (IOException ex) {
			logger.error("Error encountered while initializing config file", ex);
		}
	
		return props;
	}

	protected abstract File createConfigFile(String configFileName) throws FileNotFoundException;

}