package com.ucop.tes.customersite.config.loader;

import java.io.File;
import java.io.FileNotFoundException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class GlassfishConfigLoader extends AbstractConfigLoader {
	private static final Logger logger = LogManager.getLogger(GlassfishConfigLoader.class);
	
	private static final String GLASSFISH_INSTANCE_ROOT = "com.sun.aas.instanceRoot";
	
	public GlassfishConfigLoader() {}
	
	public File createConfigFile(String configFileName) throws FileNotFoundException {
		String instanceRoot = System.getProperty(GLASSFISH_INSTANCE_ROOT);
		
		if (instanceRoot == null) 
			throw new FileNotFoundException(
				String.format("Cannot find Glassfish instanceRoot. Make sure the %s system property is set", GLASSFISH_INSTANCE_ROOT));
		
		File configFolder = new File(instanceRoot + File.separator + "config");
		
		File configFile = new File(configFolder + File.separator + configFileName);
		
		logger.debug("Config file absolute path is: " + configFile.getAbsolutePath());
		
		return configFile;
	}

}