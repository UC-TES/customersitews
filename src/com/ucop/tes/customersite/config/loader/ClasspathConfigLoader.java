package com.ucop.tes.customersite.config.loader;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class ClasspathConfigLoader extends AbstractConfigLoader {
	private static final Logger logger = LogManager.getLogger(ClasspathConfigLoader.class);
	
	private static final String CLASSPATH_ROOT = "/";
	
	public ClasspathConfigLoader() {}
	
	public File createConfigFile(String configFileName) throws FileNotFoundException {
		URL url = ClasspathConfigLoader.class.getResource(CLASSPATH_ROOT + configFileName);
		
		if (url == null)
			throw new FileNotFoundException(String.format("Cannot find %s on classpath.", configFileName));
		
		File configFile = new File(url.getFile());
		
		logger.debug("Config file absolute path is: " + configFile.getAbsolutePath());
		
		return configFile;
	}
	
}