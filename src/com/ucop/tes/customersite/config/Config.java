package com.ucop.tes.customersite.config;

import java.util.Properties;

import com.ucop.tes.customersite.config.loader.ClasspathConfigLoader;
import com.ucop.tes.customersite.config.loader.GlassfishConfigLoader;


public class Config {
	private static final String CONFIG_FILE = "CustomerSiteWS.properties";
	
	private static Properties properties;
	
	static {
		properties = new GlassfishConfigLoader().loadProperties(CONFIG_FILE);
		//properties = new ClasspathConfigLoader().loadProperties(CONFIG_FILE);
	}
	
	private Config(){}
	
	public static String getStringProperty(String propertyName) {
		return getPropertyValue(propertyName);
	}
	
	public static boolean getBooleanProperty(String propertyName) {
		String value = getPropertyValue(propertyName);
		
		return Boolean.valueOf(value);
	}
	
	private static String getPropertyValue(String propertyName) {
		return properties.getProperty(propertyName);
	}

}