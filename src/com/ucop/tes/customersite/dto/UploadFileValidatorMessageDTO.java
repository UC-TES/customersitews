package com.ucop.tes.customersite.dto;

import java.io.Serializable;
import java.util.List;

public class UploadFileValidatorMessageDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String atpCode;
	private List<String> fileName;
	private String excelFileName;
	private String userName;
	private String requestor; //Manual or Auto
	private int submissionId; //school submission ID;
	
	
	public String getAtpCode() {
		return atpCode;
	}
	public void setAtpCode(String atpCode) {
		this.atpCode = atpCode;
	}
	public List<String> getFileName() {
		return fileName;
	}
	public void setFileName(List<String> fileName) {
		this.fileName = fileName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRequestor() {
		return requestor;
	}
	public void setRequestor(String requestor) {
		this.requestor = requestor;
	}
	public int getSubmissionId() {
		return submissionId;
	}
	public void setSubmissionId(int submissionId) {
		this.submissionId = submissionId;
	}
	public String getExcelFileName() {
		return excelFileName;
	}
	public void setExcelFileName(String excelFileName) {
		this.excelFileName = excelFileName;
	}
	
	
}
