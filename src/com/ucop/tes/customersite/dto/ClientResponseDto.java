package com.ucop.tes.customersite.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ClientResponseDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String submissionGroupId;

	public String getSubmissionGroupId() {
		return submissionGroupId;
	}

	public void setSubmissionGroupId(String submissionGroupId) {
		this.submissionGroupId = submissionGroupId;
	}

}
