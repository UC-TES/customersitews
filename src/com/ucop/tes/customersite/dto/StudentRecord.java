/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ucop.tes.customersite.dto;

import java.util.List;

/**
 *
 * @author mkgolfer
 */
public class StudentRecord extends TesRecord {

    /**
     * The number of fields in a student record
     */
    public final static int NUMBER_OF_FIELDS = 31;

//    public StudentRecord() {
//        this.courses = []
//        this.tests = []
//    }
    /**
     * The program name the student belongs to.
     */
    private String programName;

    /**
     * The school atp code the student belongs to.
     */
    private String schoolAtpCode;

    /**
     * The local student ID.
     */
    private String studentId;

    /**
     * The UC application ID.
     */
    private String ucApplicationId;

    /**
     * The CSU application ID.
     */
    private String csuApplicationId;

    /**
     * The CA State student ID (formerly CSIS Student ID.
     */
    private String caStateStudentId;

    /**
     * The student's social security number.
     */
    private String ssn;

    /**
     * The student's first name.
     */
    private String firstName;

    /**
     * The student's middle name.
     */
    private String middleName;

    /**
     * The student's last name.
     */
    private String lastName;

    /**
     * The student's address line 1.
     */
    private String address1;

    /**
     * The student's address line 2.
     */
    private String address2;

    /**
     * The student's city.
     */
    private String city;

    /**
     * The student's state.
     */
    private String state;

    /**
     * The student's zip code.
     */
    private String zip;

    /**
     * The student's phone number.
     */
    private String phone;

    /**
     * The student's date of birth.
     */
    private String dob;

    /**
     * The student's gender.
     */
    private String gender;

    /**
     * The student's grade level.
     */
    private String gradeLevel;

    /**
     * The student's local ethnicity.
     */
    private String localEthnicity;

    /**
     * The student's CBEDS ethnicity.
     */
    private String cbedsEthnicity;

    /**
     * The student's graduation date.
     */
    private String gradDate;

    /**
     * Used for new JSON format files to indicate if student has graduated
     */
    private String gradFlag;

    /**
     * The student's school GPA calculation method.
     */
    private String gpaType;

    /**
     * The student's school GPA.
     */
    private String gpa;

    /**
     * The student's class rank.
     */
    private String rank;

    /**
     * The student's meal status type.
     */
    private String mealStatusType;

    /**
     * The student's LOTE proficiency certification.
     */
    private String loteCertSource;

    /**
     * The language the student is proficient in (non-English).
     */
    private String languageCode;

    /**
     * The transcript note for the student.
     */
    private String transcriptNote;

    /**
     * The courses associated with this student.
     */
    private List<CourseRecord> courses;

    /**
     * The tests associated with this student.
     */
    private List<TestRecord> tests;

    /**
     * The Federal Ethnicity record associated with this student.
     */
    private EthnicityRecord ethnicity;
    
    private int sdYear;

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getSchoolAtpCode() {
        return schoolAtpCode;
    }

    public void setSchoolAtpCode(String schoolAtpCode) {
        this.schoolAtpCode = schoolAtpCode;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getUcApplicationId() {
        return ucApplicationId;
    }

    public void setUcApplicationId(String ucApplicationId) {
        this.ucApplicationId = ucApplicationId;
    }

    public String getCsuApplicationId() {
        return csuApplicationId;
    }

    public void setCsuApplicationId(String csuApplicationId) {
        this.csuApplicationId = csuApplicationId;
    }

    public String getCaStateStudentId() {
        return caStateStudentId;
    }

    public void setCaStateStudentId(String caStateStudentId) {
        this.caStateStudentId = caStateStudentId;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGradeLevel() {
        return gradeLevel;
    }

    public void setGradeLevel(String gradeLevel) {
        this.gradeLevel = gradeLevel;
    }

    public String getLocalEthnicity() {
        return localEthnicity;
    }

    public void setLocalEthnicity(String localEthnicity) {
        this.localEthnicity = localEthnicity;
    }

    public String getCbedsEthnicity() {
        return cbedsEthnicity;
    }

    public void setCbedsEthnicity(String cbedsEthnicity) {
        this.cbedsEthnicity = cbedsEthnicity;
    }

    public String getGradDate() {
        return gradDate;
    }

    public void setGradDate(String gradDate) {
        this.gradDate = gradDate;
    }

    public String getGradFlag() {
        return gradFlag;
    }

    public void setGradFlag(String gradFlag) {
        this.gradFlag = gradFlag;
    }

    public String getGpaType() {
        return gpaType;
    }

    public void setGpaType(String gpaType) {
        this.gpaType = gpaType;
    }

    public String getGpa() {
        return gpa;
    }

    public void setGpa(String gpa) {
        this.gpa = gpa;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getMealStatusType() {
        return mealStatusType;
    }

    public void setMealStatusType(String mealStatusType) {
        this.mealStatusType = mealStatusType;
    }

    public String getLoteCertSource() {
        return loteCertSource;
    }

    public void setLoteCertSource(String loteCertSource) {
        this.loteCertSource = loteCertSource;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getTranscriptNote() {
        return transcriptNote;
    }

    public void setTranscriptNote(String transcriptNote) {
        this.transcriptNote = transcriptNote;
    }

    public List<CourseRecord> getCourses() {
        return courses;
    }

    public void setCourses(List<CourseRecord> courses) {
        this.courses = courses;
    }

    public List<TestRecord> getTests() {
        return tests;
    }

    public void setTests(List<TestRecord> tests) {
        this.tests = tests;
    }

    public EthnicityRecord getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(EthnicityRecord ethnicity) {
        this.ethnicity = ethnicity;
    }

    public int getSdYear() {
        return sdYear;
    }

    public void setSdYear(int sdYear) {
        this.sdYear = sdYear;
    }
    
    

}
