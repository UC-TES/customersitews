/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ucop.tes.customersite.dto;

/**
 *
 * @author mkgolfer
 */
public class EthnicityRecord extends TesRecord {
     /** The number of fields in the ethnicity record */
    public final static int NUMBER_OF_FIELDS = 8;

    /** The Student Hispanic Ethnicity indicator. */
     private String hispanicEthnicity;

    /** coded values representing student's Race Category. */
     private String raceCode1;
     private String raceCode2;
     private String raceCode3;
     private String raceCode4;
     private String raceCode5;
     private String studentId;
     private String Eor;

    public String getHispanicEthnicity() {
        return hispanicEthnicity;
    }

    public void setHispanicEthnicity(String hispanicEthnicity) {
        this.hispanicEthnicity = hispanicEthnicity;
    }

    public String getRaceCode1() {
        return raceCode1;
    }

    public void setRaceCode1(String raceCode1) {
        this.raceCode1 = raceCode1;
    }

    public String getRaceCode2() {
        return raceCode2;
    }

    public void setRaceCode2(String raceCode2) {
        this.raceCode2 = raceCode2;
    }

    public String getRaceCode3() {
        return raceCode3;
    }

    public void setRaceCode3(String raceCode3) {
        this.raceCode3 = raceCode3;
    }

    public String getRaceCode4() {
        return raceCode4;
    }

    public void setRaceCode4(String raceCode4) {
        this.raceCode4 = raceCode4;
    }

    public String getRaceCode5() {
        return raceCode5;
    }

    public void setRaceCode5(String raceCode5) {
        this.raceCode5 = raceCode5;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }
    
    public String getEor(){
        return Eor;
    }
    
    public void setEor(String eor){
        this.Eor = eor;
    }
}
