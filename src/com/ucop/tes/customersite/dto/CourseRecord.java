/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ucop.tes.customersite.dto;

import java.math.BigDecimal;

/**
 *
 * @author mkgolfer
 */
public class CourseRecord  extends TesRecord{
     /** The number of fields in a course record */
    public final static int NUMBER_OF_FIELDS = 20;

    // calendar types
    public static final String QUARTER = "Q";
    public static final String SEMESTER = "S";
    public static final String TRIMESTER = "T";
    public static final String FULL = "F";
    public static final String BLOCK = "B";
    public static final String COLLEGE_QUARTER = "CQ";
    public static final String COLLEGE_SEMESTER = "CS";

    public static final BigDecimal FULL_UNIT = new BigDecimal("1");
    public static final BigDecimal SEMESTER_UNIT = new BigDecimal(".5");
    public static final BigDecimal TRIMESTER_UNIT = new BigDecimal(".3");
    public static final BigDecimal QUARTER_UNIT = new BigDecimal(".2");


    /** The grade level the course was taken in. */
     private String gradeLevel;

    /** The name of the institution where this class was taken. */
     private String schoolAttended;

    /** The School ATPCode where this class was taken. */
     private String atpCode;

    /** The County-District-School ID Number where this class was taken. */
     private String cdsCode;

    /** The school year the course was taken in. */
     private String schoolYear;

    /** The date the course was taken. */
     private String courseDate;

    /** The term for the course. */
     private String term;

    /** The block scheduling flag for the course. */
     private String blockSchedule;

    /** The work in progress flag for the course. */
     private String workInProgress;

    /** The course's local course id. */
     private String localCourseId;

    /** The course's first name. */
     private String localCourseName1;

    /** The course's alternate name. */
     private String localCourseName2;

    /** The course's other alternate name. */
     private String localCourseName3;

    /**
     * Indicates level of work that is reflected in the course, as determined by
     * the school.
     */
     private String academicIndicator;

    /** The college preparatory Indicator, as determined by the school. */
     private String collegePrepIndicator;

    /** The credit hours attemepted. */
     private String creditsAttempted;

    /** The credit hours earned. */
     private String creditsEarned;

    /** The academic grade awarded. */
     private String courseGrade;
     
     private String studentId;

    public String getGradeLevel() {
        return gradeLevel;
    }

    public void setGradeLevel(String gradeLevel) {
        this.gradeLevel = gradeLevel;
    }

    public String getSchoolAttended() {
        return schoolAttended;
    }

    public void setSchoolAttended(String schoolAttended) {
        this.schoolAttended = schoolAttended;
    }

    public String getAtpCode() {
        return atpCode;
    }

    public void setAtpCode(String atpCode) {
        this.atpCode = atpCode;
    }

    public String getCdsCode() {
        return cdsCode;
    }

    public void setCdsCode(String cdsCode) {
        this.cdsCode = cdsCode;
    }

    public String getSchoolYear() {
        return schoolYear;
    }

    public void setSchoolYear(String schoolYear) {
        this.schoolYear = schoolYear;
    }

    public String getCourseDate() {
        return courseDate;
    }

    public void setCourseDate(String courseDate) {
        this.courseDate = courseDate;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getBlockSchedule() {
        return blockSchedule;
    }

    public void setBlockSchedule(String blockSchedule) {
        this.blockSchedule = blockSchedule;
    }

    public String getWorkInProgress() {
        return workInProgress;
    }

    public void setWorkInProgress(String workInProgress) {
        this.workInProgress = workInProgress;
    }

    public String getLocalCourseId() {
        return localCourseId;
    }

    public void setLocalCourseId(String localCourseId) {
        this.localCourseId = localCourseId;
    }

    public String getLocalCourseName1() {
        return localCourseName1;
    }

    public void setLocalCourseName1(String localCourseName1) {
        this.localCourseName1 = localCourseName1;
    }

    public String getLocalCourseName2() {
        return localCourseName2;
    }

    public void setLocalCourseName2(String localCourseName2) {
        this.localCourseName2 = localCourseName2;
    }

    public String getLocalCourseName3() {
        return localCourseName3;
    }

    public void setLocalCourseName3(String localCourseName3) {
        this.localCourseName3 = localCourseName3;
    }

    public String getAcademicIndicator() {
        return academicIndicator;
    }

    public void setAcademicIndicator(String academicIndicator) {
        this.academicIndicator = academicIndicator;
    }

    public String getCollegePrepIndicator() {
        return collegePrepIndicator;
    }

    public void setCollegePrepIndicator(String collegePrepIndicator) {
        this.collegePrepIndicator = collegePrepIndicator;
    }

    public String getCreditsAttempted() {
        return creditsAttempted;
    }

    public void setCreditsAttempted(String creditsAttempted) {
        this.creditsAttempted = creditsAttempted;
    }

    public String getCreditsEarned() {
        return creditsEarned;
    }

    public void setCreditsEarned(String creditsEarned) {
        this.creditsEarned = creditsEarned;
    }

    public String getCourseGrade() {
        return courseGrade;
    }

    public void setCourseGrade(String courseGrade) {
        this.courseGrade = courseGrade;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }
     
     
    
}
