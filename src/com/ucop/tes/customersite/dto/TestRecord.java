/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ucop.tes.customersite.dto;

/**
 *
 * @author mkgolfer
 */
public class TestRecord extends TesRecord {

    /**
     * The number of fields in a test record
     */
    public final static int NUMBER_OF_FIELDS = 6;

    /**
     * The Name of Test Type.
     */
    private String testType;

    /**
     * The Date Test was Administered.
     */
    private String testDate;

    /**
     * The Name of Subtest.
     */
    private String subtestName;

    /**
     * The Test Score.
     */
    private String testscore;
    
    private String studentId;
    
    private String aptCode;

    public String getTestType() {
        return testType;
    }

    public void setTestType(String testType) {
        this.testType = testType;
    }

    public String getTestDate() {
        return testDate;
    }

    public void setTestDate(String testDate) {
        this.testDate = testDate;
    }

    public String getSubtestName() {
        return subtestName;
    }

    public void setSubtestName(String subtestName) {
        this.subtestName = subtestName;
    }

    public String getTestscore() {
        return testscore;
    }

    public void setTestscore(String testscore) {
        this.testscore = testscore;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getAptCode() {
        return aptCode;
    }

    public void setAptCode(String aptCode) {
        this.aptCode = aptCode;
    }
    
    

}
