/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ucop.tes.customersite.dto;

import java.util.List;

/**
 *
 * @author mkgolfer
 */
public class SchoolRecord extends TesRecord {

    /**
     * The number of fields in a school record
     */
    public final static int NUMBER_OF_FIELDS = 51; /**
             * The technical specification record layout version number.
             */
    private double layoutVersion; /**
             * The submission type identifier.
             */
    private String submissionType; /**
             * The school year.
             */
    private String schoolYear; /**
             * The school information system type.
             */
    private String sisType; /**
             * The additional information on SIS Type, including version,
             * database format, etc.
             */
    private String sisInfo; /**
             * The name of the extract software vendor.
             */
    private String extractVendorName; /**
             * The phone number of the extract software vendor.
             */
    private String extractVendorPhone; /**
             * The flag to identify this school as a special program.
             */
    private String programIdentifier; /**
             * The name of this program.
             */
    private String programName; /**
             * The school atp code.
             */
    private String schoolAtpcode; /**
             * The key used to authenticate ATP to School.
             */
    private String authenticationKey; /**
             * The school's county-district-school ID number.
             */
    private String cdsCode; /**
             * The school name.
             */
    private String physicalSchoolName; /**
             * The school address.
             */
    private String schoolAddress; /**
             * The school city.
             */
    private String schoolCity; /**
             * The school state.
             */
    private String schoolState; /**
             * The school zip.
             */
    private String schoolZip; /**
             * The school phone number.
             */
    private String schoolPhone; /**
             * The record fax number.
             */
    private String schoolFax; /**
             * The administrative contact first name.
             */
    private String adminContactFirstName; /**
             * The administrative contact last name.
             */
    private String adminContactLastName; /**
             * The administrative contact phone number.
             */
    private String adminContactPhone; /**
             * The administrative contact email address.
             */
    private String adminContactEmail; /**
             * The technical contact first name.
             */
    private String techContactFirstName; /**
             * The technical contact last name.
             */
    private String techContactLastName; /**
             * The technical contact phone number.
             */
    private String techContactPhone; /**
             * The technical contact email address.
             */
    private String techContactEmail; /**
             * The number of transcripts submitted for this program.
             */
    private int numTranscripts; /**
             * The number of local SIS credits that equate to one full academic
             * year of instruction.
             */
    private int carnegieUnitConversionFactor; //double carnegieUnitConversionFactor
            /**
             * The grading system this year.
             */
    private String gradingSystem; /**
             * The calendar type this year.
             */
    private String calTypeCurrentYear; /**
             * The calendar type last year.
             */
    private String calTypeLastYear; /**
             * The calendar type 2 years ago.
             */
    private String calType2YrsAgo; /**
             * The calendar type 3 years ago.
             */
    private String calType3YrsAgo; /**
             * The month of the year that the school year started on this year.
             */
    private String schoolYearStartMonthCurrentYear; /**
             * The month of the year that the school year ended on this year.
             */
    private String schoolYearEndMonthCurrentYear; /**
             * The month of the year that the school year started on last year.
             */
    private String schoolYearStartMonthLastYear; /**
             * The month of the year that the school year ended on last this
             * year.
             */
    private String schoolYearEndMonthLastYear; /**
             * The month of the year that the school year started on 2 years
             * ago.
             */
    private String schoolYearStartMonth2YrsAgo; /**
             * The month of the year that the school year ended on 2 years ago.
             */
    private String schoolYearEndMonth2YrsAgo; /**
             * The month of the year that the school year started on 3 years
             * ago.
             */
    private String schoolYearStartMonth3YrsAgo; /**
             * The month of the year that the school year ended on 3 years ago.
             */
    private String schoolYearEndMonth3YrsAgo; /**
             * The number of 9th graders enrolled in this program.
             */
    private int programEnrollment09; /**
             * The number of 10th graders enrolled in this program.
             */
    private int programEnrollment10; /**
             * The number of 11th graders enrolled in this program.
             */
    private int programEnrollment11; /**
             * The number of 12th graders enrolled in this program.
             */
    private int programEnrollment12; /**
             * Identifies which of the 3 LOCAL_COURSE_NAME fields in the Course
             * Record is used to match to the Doorways Database. This should be
             * the Course Name printed on the school's paper transcripts.
             */
    private int doorwaysMatchingCourseName; /**
             * The comment for the school.
             */
    private String schoolComment; /**
             * The date of the extract.
             */
    private String extractDate;
    
    private List<StudentRecord> students;

    public double getLayoutVersion() {
        return layoutVersion;
    }

    public void setLayoutVersion(double layoutVersion) {
        this.layoutVersion = layoutVersion;
    }

    public String getSubmissionType() {
        return submissionType;
    }

    public void setSubmissionType(String submissionType) {
        this.submissionType = submissionType;
    }

    public String getSchoolYear() {
        return schoolYear;
    }

    public void setSchoolYear(String schoolYear) {
        this.schoolYear = schoolYear;
    }

    public String getSisType() {
        return sisType;
    }

    public void setSisType(String sisType) {
        this.sisType = sisType;
    }

    public String getSisInfo() {
        return sisInfo;
    }

    public void setSisInfo(String sisInfo) {
        this.sisInfo = sisInfo;
    }

    public String getExtractVendorName() {
        return extractVendorName;
    }

    public void setExtractVendorName(String extractVendorName) {
        this.extractVendorName = extractVendorName;
    }

    public String getExtractVendorPhone() {
        return extractVendorPhone;
    }

    public void setExtractVendorPhone(String extractVendorPhone) {
        this.extractVendorPhone = extractVendorPhone;
    }

    public String getProgramIdentifier() {
        return programIdentifier;
    }

    public void setProgramIdentifier(String programIdentifier) {
        this.programIdentifier = programIdentifier;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getSchoolAtpcode() {
        return schoolAtpcode;
    }

    public void setSchoolAtpcode(String schoolAtpcode) {
        this.schoolAtpcode = schoolAtpcode;
    }

    public String getAuthenticationKey() {
        return authenticationKey;
    }

    public void setAuthenticationKey(String authenticationKey) {
        this.authenticationKey = authenticationKey;
    }

    public String getCdsCode() {
        return cdsCode;
    }

    public void setCdsCode(String cdsCode) {
        this.cdsCode = cdsCode;
    }

    public String getPhysicalSchoolName() {
        return physicalSchoolName;
    }

    public void setPhysicalSchoolName(String physicalSchoolName) {
        this.physicalSchoolName = physicalSchoolName;
    }

    public String getSchoolAddress() {
        return schoolAddress;
    }

    public void setSchoolAddress(String schoolAddress) {
        this.schoolAddress = schoolAddress;
    }

    public String getSchoolCity() {
        return schoolCity;
    }

    public void setSchoolCity(String schoolCity) {
        this.schoolCity = schoolCity;
    }

    public String getSchoolState() {
        return schoolState;
    }

    public void setSchoolState(String schoolState) {
        this.schoolState = schoolState;
    }

    public String getSchoolZip() {
        return schoolZip;
    }

    public void setSchoolZip(String schoolZip) {
        this.schoolZip = schoolZip;
    }

    public String getSchoolPhone() {
        return schoolPhone;
    }

    public void setSchoolPhone(String schoolPhone) {
        this.schoolPhone = schoolPhone;
    }

    public String getSchoolFax() {
        return schoolFax;
    }

    public void setSchoolFax(String schoolFax) {
        this.schoolFax = schoolFax;
    }

    public String getAdminContactFirstName() {
        return adminContactFirstName;
    }

    public void setAdminContactFirstName(String adminContactFirstName) {
        this.adminContactFirstName = adminContactFirstName;
    }

    public String getAdminContactLastName() {
        return adminContactLastName;
    }

    public void setAdminContactLastName(String adminContactLastName) {
        this.adminContactLastName = adminContactLastName;
    }

    public String getAdminContactPhone() {
        return adminContactPhone;
    }

    public void setAdminContactPhone(String adminContactPhone) {
        this.adminContactPhone = adminContactPhone;
    }

    public String getAdminContactEmail() {
        return adminContactEmail;
    }

    public void setAdminContactEmail(String adminContactEmail) {
        this.adminContactEmail = adminContactEmail;
    }

    public String getTechContactFirstName() {
        return techContactFirstName;
    }

    public void setTechContactFirstName(String techContactFirstName) {
        this.techContactFirstName = techContactFirstName;
    }

    public String getTechContactLastName() {
        return techContactLastName;
    }

    public void setTechContactLastName(String techContactLastName) {
        this.techContactLastName = techContactLastName;
    }

    public String getTechContactPhone() {
        return techContactPhone;
    }

    public void setTechContactPhone(String techContactPhone) {
        this.techContactPhone = techContactPhone;
    }

    public String getTechContactEmail() {
        return techContactEmail;
    }

    public void setTechContactEmail(String techContactEmail) {
        this.techContactEmail = techContactEmail;
    }

    public int getNumTranscripts() {
        return numTranscripts;
    }

    public void setNumTranscripts(int numTranscripts) {
        this.numTranscripts = numTranscripts;
    }

    public int getCarnegieUnitConversionFactor() {
        return carnegieUnitConversionFactor;
    }

    public void setCarnegieUnitConversionFactor(int carnegieUnitConversionFactor) {
        this.carnegieUnitConversionFactor = carnegieUnitConversionFactor;
    }

    public String getGradingSystem() {
        return gradingSystem;
    }

    public void setGradingSystem(String gradingSystem) {
        this.gradingSystem = gradingSystem;
    }

    public String getCalTypeCurrentYear() {
        return calTypeCurrentYear;
    }

    public void setCalTypeCurrentYear(String calTypeCurrentYear) {
        this.calTypeCurrentYear = calTypeCurrentYear;
    }

    public String getCalTypeLastYear() {
        return calTypeLastYear;
    }

    public void setCalTypeLastYear(String calTypeLastYear) {
        this.calTypeLastYear = calTypeLastYear;
    }

    public String getCalType2YrsAgo() {
        return calType2YrsAgo;
    }

    public void setCalType2YrsAgo(String calType2YrsAgo) {
        this.calType2YrsAgo = calType2YrsAgo;
    }

    public String getCalType3YrsAgo() {
        return calType3YrsAgo;
    }

    public void setCalType3YrsAgo(String calType3YrsAgo) {
        this.calType3YrsAgo = calType3YrsAgo;
    }

    public String getSchoolYearStartMonthCurrentYear() {
        return schoolYearStartMonthCurrentYear;
    }

    public void setSchoolYearStartMonthCurrentYear(String schoolYearStartMonthCurrentYear) {
        this.schoolYearStartMonthCurrentYear = schoolYearStartMonthCurrentYear;
    }

    public String getSchoolYearEndMonthCurrentYear() {
        return schoolYearEndMonthCurrentYear;
    }

    public void setSchoolYearEndMonthCurrentYear(String schoolYearEndMonthCurrentYear) {
        this.schoolYearEndMonthCurrentYear = schoolYearEndMonthCurrentYear;
    }

    public String getSchoolYearStartMonthLastYear() {
        return schoolYearStartMonthLastYear;
    }

    public void setSchoolYearStartMonthLastYear(String schoolYearStartMonthLastYear) {
        this.schoolYearStartMonthLastYear = schoolYearStartMonthLastYear;
    }

    public String getSchoolYearEndMonthLastYear() {
        return schoolYearEndMonthLastYear;
    }

    public void setSchoolYearEndMonthLastYear(String schoolYearEndMonthLastYear) {
        this.schoolYearEndMonthLastYear = schoolYearEndMonthLastYear;
    }

    public String getSchoolYearStartMonth2YrsAgo() {
        return schoolYearStartMonth2YrsAgo;
    }

    public void setSchoolYearStartMonth2YrsAgo(String schoolYearStartMonth2YrsAgo) {
        this.schoolYearStartMonth2YrsAgo = schoolYearStartMonth2YrsAgo;
    }

    public String getSchoolYearEndMonth2YrsAgo() {
        return schoolYearEndMonth2YrsAgo;
    }

    public void setSchoolYearEndMonth2YrsAgo(String schoolYearEndMonth2YrsAgo) {
        this.schoolYearEndMonth2YrsAgo = schoolYearEndMonth2YrsAgo;
    }

    public String getSchoolYearStartMonth3YrsAgo() {
        return schoolYearStartMonth3YrsAgo;
    }

    public void setSchoolYearStartMonth3YrsAgo(String schoolYearStartMonth3YrsAgo) {
        this.schoolYearStartMonth3YrsAgo = schoolYearStartMonth3YrsAgo;
    }

    public String getSchoolYearEndMonth3YrsAgo() {
        return schoolYearEndMonth3YrsAgo;
    }

    public void setSchoolYearEndMonth3YrsAgo(String schoolYearEndMonth3YrsAgo) {
        this.schoolYearEndMonth3YrsAgo = schoolYearEndMonth3YrsAgo;
    }

    public int getProgramEnrollment09() {
        return programEnrollment09;
    }

    public void setProgramEnrollment09(int programEnrollment09) {
        this.programEnrollment09 = programEnrollment09;
    }

    public int getProgramEnrollment10() {
        return programEnrollment10;
    }

    public void setProgramEnrollment10(int programEnrollment10) {
        this.programEnrollment10 = programEnrollment10;
    }

    public int getProgramEnrollment11() {
        return programEnrollment11;
    }

    public void setProgramEnrollment11(int programEnrollment11) {
        this.programEnrollment11 = programEnrollment11;
    }

    public int getProgramEnrollment12() {
        return programEnrollment12;
    }

    public void setProgramEnrollment12(int programEnrollment12) {
        this.programEnrollment12 = programEnrollment12;
    }

    public int getDoorwaysMatchingCourseName() {
        return doorwaysMatchingCourseName;
    }

    public void setDoorwaysMatchingCourseName(int doorwaysMatchingCourseName) {
        this.doorwaysMatchingCourseName = doorwaysMatchingCourseName;
    }

    public String getSchoolComment() {
        return schoolComment;
    }

    public void setSchoolComment(String schoolComment) {
        this.schoolComment = schoolComment;
    }

    public String getExtractDate() {
        return extractDate;
    }

    public void setExtractDate(String extractDate) {
        this.extractDate = extractDate;
    }

    public List<StudentRecord> getStudents() {
        return students;
    }

    public void setStudents(List<StudentRecord> students) {
        this.students = students;
    }
    
}
