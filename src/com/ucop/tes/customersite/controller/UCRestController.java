package com.ucop.tes.customersite.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.ucop.tes.customersite.domain.BatchProcessQueueRecord;
import com.ucop.tes.customersite.domain.SchoolElectronicSubmissionRecord;
import com.ucop.tes.customersite.domain.StatusRecord;
import com.ucop.tes.customersite.domain.StatusRequestRecord;
import com.ucop.tes.customersite.domain.ThresholdRecord;
import com.ucop.tes.customersite.dto.ClientDataDto;
import com.ucop.tes.customersite.dto.ClientResponseDto;
import com.ucop.tes.customersite.dto.DeleteShareFileDto;
import com.ucop.tes.customersite.dto.UploadFileValidatorMessageDTO;
import com.ucop.tes.customersite.services.BackendService;
import com.ucop.tes.customersite.services.StatusService;

@Stateless
@Path("/services")
public class UCRestController {
	private static Logger logger = Logger.getLogger(UCRestController.class);
	
	@EJB
	private StatusService statusService;
	
	@EJB
	private BackendService backendService;
	
	@POST
	@Path("/file-upload-status")
	@Produces("application/json")
	public List<SchoolElectronicSubmissionRecord> fileUploadStatus(List<StatusRequestRecord> requestRecord) {
		logger.debug("Processing Request /file-upload-status");
		
		return statusService.getFileStatus(requestRecord);
	 }
	
	@POST
	@Path("/batch-process-status")
	@Produces("application/json")
	public List<BatchProcessQueueRecord> batchProcessStatus(List<StatusRequestRecord> requestRecord) {
		logger.debug("Processing Request /batch-process-status");
		
		return statusService.getProcessStatus(requestRecord);
	 }
	
	@POST
	@Path("/threshold-status")
	@Produces("application/json")
	public List<ThresholdRecord> thresholdStatus(List<StatusRequestRecord> requestRecord) {
		logger.debug("Processing Request /threshold-status");
		
		return statusService.getThreshold(requestRecord);
	 }
	
	//MEK TES-131
	@POST
	@Path("/get-status")
	@Produces("application/json")
	public List<StatusRecord> getStatus(List<StatusRequestRecord> requestRecord ) {
		logger.info("Processing Request /get-status");
		
		List<StatusRecord> statusRecord = null;
		
		try{
			statusRecord = statusService.getStatus(requestRecord);
		} catch (Exception e){
			statusRecord = new ArrayList<StatusRecord>();
		}
				
		//return statusService.getStatus(requestRecord);
		return statusRecord;
		
	}
	
	@GET
	@Path("/create-access-db/{atpCode}") 
	public void createAccessDb(@PathParam("atpCode") String atpCode) {
		logger.debug(String.format("Processing Request /create-access-db/%s", atpCode));
		
		backendService.createMSAccessDB(new String[] {atpCode}); 
	}
	 
	
	/*
	@GET
	@Path("/create-access-db/{atpCode}")
	public void createAccessDb(@PathParam("atpCode") String[] atpCode) {
		logger.debug(String.format("Processing Request /create-access-db/%s", atpCode[0]));
		
		System.out.println(String.format(">>>> Processing Request /create-access-db/%s", atpCode[0]));
		
		backendService.createMSAccessDB(atpCode);
	}
	*/
	
		
	@GET
	@Path("/get-access-db-status/{atpCode}") 
	@Produces("application/json")
	public List<BatchProcessQueueRecord> getAccessDbStatus(@PathParam("atpCode") String atpCode) {
		logger.debug(String.format("Processing Request /get-access-db-status/%s", atpCode));
		
		return statusService.getAccessDbStauts(new String[] {atpCode}); 
	 }
	 
	
	/*
	 * @GET
	 * 
	 * @Path("/get-access-db-status/{atpCode}") public List<BatchProcessQueueRecord>
	 * getAccessDbStatus(@PathParam("atpCode") String[] atpCode){
	 * logger.debug(String.format("Processing Request /get-access-db-status/%s",
	 * atpCode[0]));
	 * 
	 * return statusService.getAccessDbStauts(atpCode); }
	 */
	
	@GET
	@Path("/create-common-name")
	@Produces("application/json")
	public String createCommonName(){
		logger.debug("Processing Request /create-common-name");
		
		return backendService.commonNameCreator();
	}
	
	@POST
	@Path("/delete-file")
	public boolean deleteShareFileDirectory(DeleteShareFileDto fileToDelete) {
		logger.debug("Processing Request /delete-file");
		
		boolean rtnValue = false;
		
		if(backendService.deleteShareDirectoryFile(fileToDelete.getFileName())){
			rtnValue = true;
		} else {
			rtnValue = false;
		}
		
		return rtnValue;
		
	}
	
	@POST
	@Path("/copy-file")
	public Response copyFromAws(ClientDataDto fileCopyRequest){
		logger.debug("Processing Request /copy-file");
		
		UploadFileValidatorMessageDTO dto = new UploadFileValidatorMessageDTO();
		dto.setAtpCode(fileCopyRequest.getAtpCode());
		dto.setFileName(fileCopyRequest.getFilenames());
		dto.setRequestor(fileCopyRequest.getBatchMode());
		dto.setUserName(fileCopyRequest.getEmailAddress());
		
		String returnValue = backendService.fileUploadValidator(dto); 
		
		ClientResponseDto responseDto = new ClientResponseDto();
		
		//MEK if return is NOT a valid UUID, the customer site requires we send back a null in the UUID field
		if(!isValidUuId(returnValue)){
			responseDto.setSubmissionGroupId(null);
		}
		else {
			responseDto.setSubmissionGroupId(returnValue);
		}
		
		return Response.status(Response.Status.OK)
			      	   .entity(responseDto)
			      	   .type(MediaType.APPLICATION_JSON)
			      	   .build();
	}
	
	private boolean isValidUuId(String uuId) {
		logger.debug("Validating UUID: " + uuId);
		
		try {
			
			UUID.fromString(uuId);
    
        } catch (IllegalArgumentException iae) {
            return false;
        }
		
		return true;	
	}
}
