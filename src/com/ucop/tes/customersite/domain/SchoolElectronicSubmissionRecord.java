package com.ucop.tes.customersite.domain;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class SchoolElectronicSubmissionRecord implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int autoElectronicSubmissionsID;
	private String atpCode;
	private Date dateSubmitted;
	private String fileName;
	private int numberOfTranscripts;
	private String loadedFlag;
	private Date dateLoaded;
	private String programId;
	private String layoutVersion;
	private String sisType;
	private String sisInfo;
	private String extractVendorName;
	private String extractVendorPhone;
	private String adminContactFName;
	private String adminContactLName;
	private String adminContactEmail;
	private String processingStatus;
	private String systemUuId;
	
	
	public int getAutoElectronicSubmissionsID() {
		return autoElectronicSubmissionsID;
	}
	public void setAutoElectronicSubmissionsID(int autoElectronicSubmissionsID) {
		this.autoElectronicSubmissionsID = autoElectronicSubmissionsID;
	}
	public String getAtpCode() {
		return atpCode;
	}
	public void setAtpCode(String atpCode) {
		this.atpCode = atpCode;
	}
	public Date getDateSubmitted() {
		return dateSubmitted;
	}
	public void setDateSubmitted(Date dateSubmitted) {
		this.dateSubmitted = dateSubmitted;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public int getNumberOfTranscripts() {
		return numberOfTranscripts;
	}
	public void setNumberOfTranscripts(int numberOfTranscripts) {
		this.numberOfTranscripts = numberOfTranscripts;
	}
	public String getLoadedFlag() {
		return loadedFlag;
	}
	public void setLoadedFlag(String loadedFlag) {
		this.loadedFlag = loadedFlag;
	}
	public Date getDateLoaded() {
		return dateLoaded;
	}
	public void setDateLoaded(Date dateLoaded) {
		this.dateLoaded = dateLoaded;
	}
	public String getProgramId() {
		return programId;
	}
	public void setProgramId(String programId) {
		this.programId = programId;
	}
	public String getLayoutVersion() {
		return layoutVersion;
	}
	public void setLayoutVersion(String layoutVersion) {
		this.layoutVersion = layoutVersion;
	}
	public String getSisType() {
		return sisType;
	}
	public void setSisType(String sisType) {
		this.sisType = sisType;
	}
	public String getSisInfo() {
		return sisInfo;
	}
	public void setSisInfo(String sisInfo) {
		this.sisInfo = sisInfo;
	}
	public String getExtractVendorName() {
		return extractVendorName;
	}
	public void setExtractVendorName(String extractVendorName) {
		this.extractVendorName = extractVendorName;
	}
	public String getExtractVendorPhone() {
		return extractVendorPhone;
	}
	public void setExtractVendorPhone(String extractVendorPhone) {
		this.extractVendorPhone = extractVendorPhone;
	}
	public String getAdminContactFName() {
		return adminContactFName;
	}
	public void setAdminContactFName(String adminContactFName) {
		this.adminContactFName = adminContactFName;
	}
	public String getAdminContactLName() {
		return adminContactLName;
	}
	public void setAdminContactLName(String adminContactLName) {
		this.adminContactLName = adminContactLName;
	}
	public String getAdminContactEmail() {
		return adminContactEmail;
	}
	public void setAdminContactEmail(String adminContactEmail) {
		this.adminContactEmail = adminContactEmail;
	}
	public String getProcessingStatus() {
		return processingStatus;
	}
	public void setProcessingStatus(String processingStatus) {
		this.processingStatus = processingStatus;
	}
	public String getSystemUuId() {
		return systemUuId;
	}
	public void setSystemUuId(String systemUuId) {
		this.systemUuId = systemUuId;
	}
		
	
}
