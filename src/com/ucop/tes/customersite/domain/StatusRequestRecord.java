package com.ucop.tes.customersite.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class StatusRequestRecord implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String atpCode;
	private String systemUuId;
	
	public String getAtpCode() {
		return atpCode;
	}
	public void setAtpCode(String atpCode) {
		this.atpCode = atpCode;
	}
	public String getSystemUuId() {
		return systemUuId;
	}
	public void setSystemUuId(String systemUuId) {
		this.systemUuId = systemUuId;
	}
	
	

}
