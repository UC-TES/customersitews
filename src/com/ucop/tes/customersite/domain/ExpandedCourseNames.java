package com.ucop.tes.customersite.domain;

import java.io.Serializable;

public class ExpandedCourseNames implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String abbrevCourseName;
	
	private String expandCourseName;

	public String getAbbrevCourseName() {
		return abbrevCourseName;
	}

	public void setAbbrevCourseName(String abbrevCourseName) {
		this.abbrevCourseName = abbrevCourseName;
	}

	public String getExpandCourseName() {
		return expandCourseName;
	}

	public void setExpandCourseName(String expandCourseName) {
		this.expandCourseName = expandCourseName;
	}
	
	
	

}
