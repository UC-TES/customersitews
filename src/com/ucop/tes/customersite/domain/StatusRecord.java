package com.ucop.tes.customersite.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class StatusRecord implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String atpCode; /* school Atp Code */
	private String systemUuId; /* file processor generated UUID */
	private List<SchoolElectronicSubmissionRecord> fileStatus; /* schoolElectronicSubmissons table record */
	private String batchType; /* Manual Batch or Auto Batch */
	private String batchStatus; /* Processing status at school atp level */
	private String batchStep; /* Processing status by process step per school atp code */
	private String threshold; /* data quality threshold value */
	
	public String getAtpCode() {
		return atpCode;
	}
	public void setAtpCode(String atpCode) {
		this.atpCode = atpCode;
	}
	public String getSystemUuId() {
		return systemUuId;
	}
	public void setSystemUuId(String systemUuId) {
		this.systemUuId = systemUuId;
	}
	public List<SchoolElectronicSubmissionRecord> getFileStatus() {
		if(this.fileStatus == null){
			this.fileStatus = new ArrayList<SchoolElectronicSubmissionRecord>();
		}
		return fileStatus;
	}
	public void setFileStatus(List<SchoolElectronicSubmissionRecord> fileStatus) {
		this.fileStatus = fileStatus;
	}
	public String getBatchType() {
		return batchType;
	}
	public void setBatchType(String batchType) {
		this.batchType = batchType;
	}
	public String getBatchStatus() {
		return batchStatus;
	}
	public void setBatchStatus(String batchStatus) {
		this.batchStatus = batchStatus;
	}
	public String getBatchStep() {
		return batchStep;
	}
	public void setBatchStep(String batchStep) {
		this.batchStep = batchStep;
	}
	public String getThreshold() {
		return threshold;
	}
	public void setThreshold(String threshold) {
		this.threshold = threshold;
	} 
	
}
