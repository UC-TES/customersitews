package com.ucop.tes.customersite.domain;

import java.io.Serializable;
import java.util.Date;

public class PollerStatusDto implements Serializable {
	private static final long serialVersionUID = 3775513576202869561L;
	
	private long processStatusID;
	private long jobId;
	private String processName;
	private String status;
	private int successRecordCount;
	private int errorRecordCount;
	private Date startTime;
	private Date endTime;
	
	public long getProcessStatusID() {
		return processStatusID;
	}
	public void setProcessStatusID(long processStatusID) {
		this.processStatusID = processStatusID;
	}
	public long getJobId() {
		return jobId;
	}
	public void setJobId(long jobId) {
		this.jobId = jobId;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getSuccessRecordCount() {
		return successRecordCount;
	}
	public void setSuccessRecordCount(int successRecordCount) {
		this.successRecordCount = successRecordCount;
	}
	public int getErrorRecordCount() {
		return errorRecordCount;
	}
	public void setErrorRecordCount(int errorRecordCount) {
		this.errorRecordCount = errorRecordCount;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
}
