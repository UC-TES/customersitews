package com.ucop.tes.customersite.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BatchProcessQueueRecord implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int jobId;
	private String atpCode;
	private String jobStatus;
	private String requester;
	private int recordCount;
	private Date upLoadTime;
	private Date batchCompletionTime;
	private String systemUuId;
	private List<BatchProcessStatusRecord> batchStatus;
	
	
	public int getJobId() {
		return jobId;
	}
	public void setJobId(int jobId) {
		this.jobId = jobId;
	}
	public String getAtpCode() {
		return atpCode;
	}
	public void setAtpCode(String atpCode) {
		this.atpCode = atpCode;
	}
	public String getJobStatus() {
		return jobStatus;
	}
	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}
	public String getRequester() {
		return requester;
	}
	public void setRequester(String requester) {
		this.requester = requester;
	}
	public int getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
	}
	public Date getUpLoadTime() {
		return upLoadTime;
	}
	public void setUpLoadTime(Date upLoadTime) {
		this.upLoadTime = upLoadTime;
	}
	public Date getBatchCompletionTime() {
		return batchCompletionTime;
	}
	public void setBatchCompletionTime(Date batchCompletionTime) {
		this.batchCompletionTime = batchCompletionTime;
	}
	public List<BatchProcessStatusRecord> getBatchStatus() {
		return batchStatus;
	}
	public void setBatchStatus(List<BatchProcessStatusRecord> batchStatus) {
		this.batchStatus = batchStatus;
	}
	public String getSystemUuId() {
		return systemUuId;
	}
	public void setSystemUuId(String systemUuId) {
		this.systemUuId = systemUuId;
	}
	
	
}
