package com.ucop.tes.customersite.domain;

import java.io.Serializable;
import java.util.Date;

public class PollerQueueDto implements Serializable {
	private static final long serialVersionUID = 4975978431720499644L;
	
	private long jobId;
	private String schoolATPCode;
	private String JobStatus;
	private String requester;
	private int recordCount;
	private Date UploadTime;
	private Date batchCompletionTime;
	private String env;
	private String runType;
	private String fromQueue;
	private String toQueue;
	private int statusId;
	
	public long getJobId() {
		return jobId;
	}
	public void setJobId(long jobId) {
		this.jobId = jobId;
	}
	public String getSchoolATPCode() {
		return schoolATPCode;
	}
	public void setSchoolATPCode(String schoolATPCode) {
		this.schoolATPCode = schoolATPCode;
	}
	public String getJobStatus() {
		return JobStatus;
	}
	public void setJobStatus(String jobStatus) {
		JobStatus = jobStatus;
	}
	public int getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
	}
	public Date getUploadTime() {
		return UploadTime;
	}
	public void setUploadTime(Date uploadTime) {
		UploadTime = uploadTime;
	}
	public Date getBatchCompletionTime() {
		return batchCompletionTime;
	}
	public void setBatchCompletionTime(Date batchCompletionTime) {
		this.batchCompletionTime = batchCompletionTime;
	}
	public String getEnv() {
		return env;
	}
	public void setEnv(String env) {
		this.env = env;
	}
	public String getRunType() {
		return runType;
	}
	public void setRunType(String runType) {
		this.runType = runType;
	}
	public String getFromQueue() {
		return fromQueue;
	}
	public void setFromQueue(String fromQueue) {
		this.fromQueue = fromQueue;
	}
	public String getToQueue() {
		return toQueue;
	}
	public void setToQueue(String toQueue) {
		this.toQueue = toQueue;
	}
	public String getRequester() {
		return requester;
	}
	public void setRequester(String requester) {
		this.requester = requester;
	}
	public int getStatusId() {
		return statusId;
	}
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
	
	
}
