package com.ucop.tes.customersite.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ThresholdRecord implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String schoolAtpCode;
	private int matchingThresholdNumeric;
	private String matchingThresholdSymbolic;
	
	
	public String getSchoolAtpCode() {
		return schoolAtpCode;
	}
	public void setSchoolAtpCode(String schoolAtpCode) {
		this.schoolAtpCode = schoolAtpCode;
	}
	public int getMatchingThresholdNumeric() {
		return matchingThresholdNumeric;
	}
	public void setMatchingThresholdNumeric(int matchingThresholdNumeric) {
		this.matchingThresholdNumeric = matchingThresholdNumeric;
	}
	public String getMatchingThresholdSymbolic() {
		return matchingThresholdSymbolic;
	}
	public void setMatchingThresholdSymbolic(String matchingThresholdSymbolic) {
		this.matchingThresholdSymbolic = matchingThresholdSymbolic;
	}
	
	
	
}
