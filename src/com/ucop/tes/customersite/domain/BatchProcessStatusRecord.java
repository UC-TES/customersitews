package com.ucop.tes.customersite.domain;

import java.io.Serializable;
import java.util.Date;

public class BatchProcessStatusRecord implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int processStatusId;
	private int jobId;
	private String processName;
	private String status;
	private int successRecordCount;
	private int errorRecordCount;
	private Date startTime;
	private Date endTime;
	
	
	public int getProcessStatusId() {
		return processStatusId;
	}
	public void setProcessStatusId(int processStatusId) {
		this.processStatusId = processStatusId;
	}
	public int getJobId() {
		return jobId;
	}
	public void setJobId(int jobId) {
		this.jobId = jobId;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getSuccessRecordCount() {
		return successRecordCount;
	}
	public void setSuccessRecordCount(int successRecordCount) {
		this.successRecordCount = successRecordCount;
	}
	public int getErrorRecordCount() {
		return errorRecordCount;
	}
	public void setErrorRecordCount(int errorRecordCount) {
		this.errorRecordCount = errorRecordCount;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

}
