package com.ucop.tes.customersite.domain;

import java.io.Serializable;

public class CommonNameCreationDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String sourceCourseName;

	public String getSourceCourseName() {
		return sourceCourseName;
	}

	public void setSourceCourseName(String sourceCourseName) {
		this.sourceCourseName = sourceCourseName;
	}
	
	

}
