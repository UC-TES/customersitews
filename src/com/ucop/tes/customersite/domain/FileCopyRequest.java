package com.ucop.tes.customersite.domain;

import java.io.Serializable;

public class FileCopyRequest implements Serializable {
	private static final long serialVersionUID = -8019016077851038551L;
	
	String atpCode;
	
	String fileName;
	
	public String getAtpCode() {
		return atpCode;
	}
	public void setAtpCode(String atpCode) {
		this.atpCode = atpCode;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	

}
