package com.ucop.tes.customersite.common;

import java.util.HashMap;

public class CNCParameters {
    private static HashMap hmSynch = new HashMap() {
		{
    		put("01", "1");
    		put("09", "1");
    		put("9", "1");
    		put("I", "1");   		
    		put("02", "2");
    		put("10", "2");    		
    		put("II", "2");
    		put("03", "3");    		
    		put("11", "3");
    		put("III", "3");
    		put("04", "4");
    		put("12", "4");
    		put("IV", "4");
    		put("09", "1");    		
    	}
    };
	
	public static HashMap getHmSynch() {
		return hmSynch;
	}

	public static void setHmSynch(HashMap hmSynch) {
		CNCParameters.hmSynch = hmSynch;
	}	

}
