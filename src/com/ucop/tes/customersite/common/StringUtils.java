package com.ucop.tes.customersite.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class StringUtils {
	private static Pattern patternNonAscii ;
	private static Pattern patternSpecialCharacters;
	private static Pattern patternLetter;
	private static Pattern patternNumber;
	private static Pattern patternSynchedCharacters;
	
    static {
    	patternNonAscii = Pattern.compile(CommonNameConstants.REGEX_NON_ASCII);
    	patternSpecialCharacters = Pattern.compile(CommonNameConstants.REGEX_LETTER_SPECIALCHARACTER_LETTER + "|" +
    												CommonNameConstants.REGEX_LETTER_SPECIALCHARACTER + "|" + 
    									           CommonNameConstants.REGEX_SPECIALCHARACTER_LETTER + "|" +
    									           CommonNameConstants.REGEX_NUMBER_SPECIALCHARACTER_I + "|" +
    									           CommonNameConstants.REGEX_I_SPECIALCHARACTER_NUMBER
    									           , Pattern.CASE_INSENSITIVE);
    	patternLetter = Pattern.compile(CommonNameConstants.REGEX_LETTER, Pattern.CASE_INSENSITIVE);
    	patternNumber = Pattern.compile(CommonNameConstants.REGEX_NUMBER);
    	patternSynchedCharacters = Pattern.compile(CommonNameConstants.REGEX_SYNCHED_CHARACTER_AT_BEGINNING, Pattern.CASE_INSENSITIVE);
    }
    
    public static boolean isNumber(String value) {
    	return patternNumber.matcher(value).matches();
    }
    
	public static boolean containsNonAscii(String value) throws Exception {
		if (value == null) {
			throw new Exception("StringUtils.containsNonAscii(): No Data Available for pattern matching");
		} // end if
		
		return patternNonAscii.matcher(value).find();
	}
    
    public static String removeSpecialCharacters(String str) {
    	return replace(str, 
    				patternSpecialCharacters, 
    				CommonNameConstants.REGEX_SPECIALCHARACTER, 
    				CommonNameConstants.SPACE);
    }
    
    public static String removeSpaces(String str) {
    	return replace(str, 
    				patternLetter,
					CommonNameConstants.REGEX_SPACES,
					CommonNameConstants.EMPTY_STRING);
    }

	public static String replace(String str, Pattern patternToFind, String regexToBeReplaced, String replacement) {
		if (str == null || patternToFind == null || replacement == null) {
			return null;
		}
 
		str = removeCharactersAtHead(str, regexToBeReplaced);

		Matcher m = patternToFind.matcher(str);
		int startInput = 0;
		int startGroup = 0;
		int endGroup =0;
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			String group = m.group();
			startGroup = m.start();
			endGroup = m.end();

			group = group.replaceAll(regexToBeReplaced , replacement);
   
			sb.append(str.substring(startInput, startGroup)).append(group);
			startInput = endGroup;
		}
		String tail = str.substring(endGroup);
		sb.append(tail);

		return removeCharactersAtTail(sb.toString(), regexToBeReplaced);
	}
	
	public static String removeCharactersAtHead(String str, String regexToBeRemoved) {
		if (str == null || str.trim().length() == 0) {
			return str;
		}
		
		str = str.trim();
		
		Pattern p = Pattern.compile("\\A" + regexToBeRemoved);
		Matcher m = p.matcher(str);
		if (m.find()) {
			str = str.replaceFirst(regexToBeRemoved, " ");
		}

		return str;
	}	
	
	public static String removeCharactersAtTail(String str, String regexToBeRemoved) {
		if (str == null || str.trim().length() == 0) {
			return str;
		}
		
		str = str.trim();
		
		Pattern p = Pattern.compile(regexToBeRemoved + "\\z");
		Matcher m = p.matcher(str);
		if (m.find()) {
			String group = m.group();
			str = str.substring(0, str.length()- group.length()) + group.replaceAll(regexToBeRemoved, "");
		}	

		return str;
	}
	
	public static String reverseSynchedCharacters(String str) {
		if (str == null || str.trim().length() == 0) {
			return str;
		}

		Matcher matcher = patternSynchedCharacters.matcher(str);
    	if (matcher.find()) {
    		String group = matcher.group();
    		// move the synched characters to the end of the string
    		str = str.substring(group.length(), str.length()) + group.trim();
    	}
    	
    	return str;
	}
}
