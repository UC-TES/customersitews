package com.ucop.tes.customersite.common;

public class CommonNameConstants {
	public static final String DOORWAYS_STATIC_COURSE_RUN = "0";
	public static final String SCHOOL_TRANSCRIPT_RUN = "1";
	
	public static final String EMPTY_STRING = "";
	public static final String NEW_LINE = "\r\n";
	public static final String SPACE_A = " A";
	public static final String SPACE_B = " B";
	public static final String SPACE = " ";
	public static final String TAB = "\t";
	
	public static final String REGEX_NON_ASCII = "[^\\x00-\\x7F]";
	public static final String REGEX_SPACES = "\\s+";
	
	public static final String REGEX_LETTER = "\\s*[a-zA-Z]+\\s*";
	public static final String REGEX_NUMBER = "\\d+";
	public static final String REGEX_SPECIALCHARACTER = "\\s*([\\W\\s]\\s*)+";
	public static final String REGEX_LETTER_NON_I = "\\s*[a-zA-Z&&[^I]]+"; //Roman Numeral: I, II, III, IV
	public static final String REGEX_LETTER_SPECIALCHARACTER = REGEX_LETTER_NON_I + REGEX_SPECIALCHARACTER; 
	public static final String REGEX_SPECIALCHARACTER_LETTER = REGEX_SPECIALCHARACTER + REGEX_LETTER_NON_I; 
	public static final String REGEX_LETTER_SPECIALCHARACTER_LETTER = REGEX_SPECIALCHARACTER + REGEX_LETTER_NON_I +  REGEX_SPECIALCHARACTER;
	public static final String REGEX_NUMBER_SPECIALCHARACTER_I = "\\d+" + REGEX_SPECIALCHARACTER + "[I]";
	public static final String REGEX_I_SPECIALCHARACTER_NUMBER = "[I]" + REGEX_SPECIALCHARACTER + "\\d+";
	
	public static final String REGEX_SYNCHED_CHARACTER_AT_BEGINNING = "\\A([1-4H]\\s+)+";
}
